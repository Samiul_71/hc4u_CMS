<?php
   require_once "header.php";
   require_once 'db.php';
?>

<div class="box">
    <div class="wrapper">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="fresh-table full-color-azure">
                    <table id="fresh-table" class="table">
                        <thead>
                            <tr>
<!--                                <th data-field="serial" data-sortable="true">Serial No.</th>-->
                                <th>Hospital Name</th>
                                <th>Address</th>
                                <th>City</th>
                                <th>District</th>
                                <th>Image</th>
                                <th>Description</th>
                                <th>Discount</th>
                            </tr>
                        </thead>
                        <?php
                        $sql1 = "SELECT * FROM hospital WHERE status=1";
                        $result3 = mysqli_query($link, $sql1);
                        while ($row = mysqli_fetch_assoc($result3)) {
                            $data4[] = $row;
                        }
                        ?>
                            <tbody>
                            <?php
                            if (!empty($data4)) {
                            foreach ($data4 as $item1) {
                            ?>
                            <tr>


                                <td><?php echo  $item1['name'];?></td>
                                <td><?php echo  $item1['address'];?></td>
                                <td><?php echo  $item1['city'];?></td>
                                <td><?php echo  $item1['district'];?></td>
                                <td><img src="admin/public/<?php echo $item1['image'];?>" height="50px" width="50px"></td>
                                <td><?php echo  $item1['description'];?></td>
                                <td><?php echo  $item1['discount'];?></td>

                            </tr>
                            <?php }} ?>
                             </tbody>
                    </table>
                </div>
            </div>
        </div>

        <br><br>
        <div id="achieve">
            <div class="container">
                <br>
                <div class="row" id="pre-footer">
                    <div class="col-sm-5 col-md-5 form-inline text-center">
                        <label> News Letter:</label>
                        <input type="text" name="" placeholder="  Please enter your Email Address for Newsletter" style="border-radius: 10px; width: 330px; height: 30px;">
                    </div>
                    <div class="col-sm-3 col-md-3 text-center">
                        <div class="row text-center">
                            <a href="https://itunes.apple.com/gb/app/hc4u/id1038842422?mt=8" target="_blank" style="text-decoration: none;">
                                <img src="img/ios.png">
                            </a>
                            <a href="https://play.google.com/store/apps/details?id=com.healthcare.hc4u&hl=en" target="_blank" style="text-decoration: none;">
                                <img src="img/android.png">
                            </a>
                        </div>
                    </div>
                    <div class="col-sm-4 col-md-4 form-inline text-center">
                        <label>Social Links: </label>&nbsp;&nbsp;
                        <a href="https://twitter.com/Hc4ubd" target="_blank" style="text-decoration: none;">
                            <span><i class="fa fa-twitter-square fa-3x" aria-hidden="true"></i></span>
                            <!--<img src="img/fb.png">-->
                        </a>&nbsp;&nbsp;
                        <a href="https://www.facebook.com/hc4ubd" target="_blank" style="text-decoration: none;">
                            <span><i class="fa fa-facebook-square fa-3x" aria-hidden="true"></i></span>
                            <!--<img src="img/twitter.png">-->
                        </a>
                    </div>
                </div><br>
            </div>
        </div>
    </div>
</div>

<?php require_once "footer.php"; ?>


<script src="js/jquery-1.11.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/slick.min.js"></script>
<script src="js/placeholdem.min.js"></script>
<script src="js/rs-plugin/js/jquery.themepunch.plugins.min.js"></script>
<script src="js/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
<script src="js/waypoints.min.js"></script>
<script src="js/scripts.js"></script>


<!--<script type="text/javascript" src="js/table/jquery-1.11.2.min.js"></script>-->
<!--<script type="text/javascript" src="js/table/bootstrap.js"></script>-->
<script type="text/javascript" src="js/table/bootstrap-table.js"></script>
<script>
    $(document).ready(function() {
        appMaster.preLoader();
    });
</script>

<script type="text/javascript">
    var $table = $('#fresh-table');

    $().ready(function(){
        $table.bootstrapTable({
            toolbar: ".toolbar",

            showRefresh: true,
            search: true,
            showToggle: true,
            showColumns: true,
            pagination: true,
            striped: true,
            pageSize: 10,
            pageList: [10,25,50,100],

            formatShowingRows: function(pageFrom, pageTo, totalRows){
                //do nothing here, we don't want to show the text "showing x of y from..."
            },
            formatRecordsPerPage: function(pageNumber){
                return pageNumber + " Rows Visible";
            }
        });

        $(window).resize(function () {
            $table.bootstrapTable('resetView');
        });
    });

</script>

</body>
</html>
