<?php ?>

<!doctype html>
<html lang="en" class="no-js">
    <head>
        <meta charset="UTF-8">
        <title>About Us - Health Care For You</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

        <link rel="shortcut icon" href="favicon2.png">
        <link rel="stylesheet" href="css/bootstrap.css">

        <link rel="stylesheet" href="css/animate.css">
        <link rel="stylesheet" href="css/font-awesome.min.css">
        <link rel="stylesheet" href="css/slick.css">
        <link rel="stylesheet" href="js/rs-plugin/css/settings.css">

        <script type="text/javascript" src="js/modernizr.custom.32033.js"></script>

        <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet">

        <link rel="stylesheet" href="css/eco.css">
        <link href="css/style.css" rel="stylesheet" type="text/css">

        <style type="text/css">
            #content:after {
                content : "";
                display: block;
                position: absolute;
                top: 0;
                left: 0;
                background-image: url(img/bg_images/footer.jpg);
                width: 100%;
                height: 100%;
                opacity : 0.2;
                z-index: -1;
            }
            #achieve {
                position: relative;
                color: #d7a10a;
            }
            #achieve:after {
                content : "";
                display: block;
                position: absolute;
                top: 0;
                left: 0;
                background-image: url(img/bg_images/footer.jpg);
                width: 100%;
                height: 100%;
                opacity : 0.9;
                z-index: -1;
            }

            #about {
                padding-top: 140px;
                margin-top: -140px;
            }
            #contact {
                padding-top: 140px;
                margin-top: -140px;
            }
            #condition {
                padding-top: 140px;
                margin-top: -140px;
            }
        </style>
        <style>
            #map {
                border: dashed;
                border-radius: 5px;
                width: 1000px;
                height: 300px;
            }
            footer h4{
                color: white;
            }
            footer a{
                color: white;
            }
        </style>
    </head>
    <body>
        <div class="pre-loader">
            <div class="load-con">
                <img src="img/logo_bk.png" class="animated fadeInDown" alt="">
                <div class="spinner">
                    <div class="bounce1"></div>
                    <div class="bounce2"></div>
                    <div class="bounce3"></div>
                </div>
            </div>
        </div>
        <header>
            <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
                <div class="container">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                            <span class="fa fa-bars fa-lg"></span>
                        </button>
                        <a class="navbar-brand" href="index.php">
                            <img src="img/logo_bk.png" alt="" >
                        </a>
                    </div>
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav navbar-right">
                            <li>
                                <form class="navbar-form navbar-left" role="search">
                                    <div class="form-group">
                                        <i class="icon-search"></i>
                                        <input type="text" placeholder="   Search" style="border-radius: 10px; width: 330px; height: 30px;">
                                    </div>
                                </form>
                            </li>
                            <li><a href="index.php" class="glyphicon glyphicon-home"> Home </a></li>
                            <li>
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Partner<span class="caret"></span></a>
                                <ul class="dropdown-menu text-right">
                                    <li class="text-right"><a href="hospitals.php">Hospital List</a></li>
                                    <li class="text-right"><a href="corporation.php#teletalk">Corporation</a></li>

                                </ul>
                            </li>
                            <li>
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Products<span class="caret"></span></a>
                                <ul class="dropdown-menu text-right">
                                    <li class="text-right"><a href="products.php">HC4U</a></li>
                                    <li class="text-right"><a href="products.php">TeleDaktar</a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Events<span class="caret"></span></a>
                                <ul class="dropdown-menu text-right" id="dropdwon-menu">
                                    <li class="text-right"><a href="events.php#csr">CSR</a></li>
                                    <li class="text-right"><a href="events.php#fair">Fair</a></li>
                                    <li class="text-right"><a href="events.php#competition">Competition</a></li>
                                    <li class="text-right"><a href="events.php#upcoming_events">Upcoming Events</a></li>
                                </ul>
                            </li>
                            <li><a href="http://healthcare4umember.com">My Account</a></li>
                        </ul>
                    </div>
                </div>
            </nav>
        </header>

        <section id="content">
            <div class="container">
                <div class="section-heading scrollpoint sp-effect3">
                    <h2>Information</h2>
                    <div class="divider"></div>
                    <p>Health Care For You (Pvt) Ltd. (HC4U)</p>
                </div>
                <div class="row" id="about">
                    <div class="col-md-12 col-sm-12">
                        <h3>About Us</h3>
                        <p>
                            Heath Care For You (Pvt) Ltd., a technology based company, is working on the next generation of healthcare for the nation. We have developed a web-based mobile application, HC4U.
                            <br><br>
                            HC4U is an IT platform to empower everyone for taking charge of his/her own health using mHealth. This platform integrates health-care seekers with healthcare providers such as hospitals, doctors and insurance companies under one umbrella offering opportunity for a paperless & cashless treatment for every individual under the universal wellness program.
                            <br><br>
                            HC4U is a movement to develop national health backbone with personal healthcare records of every citizen into an inter-operable system. It raises awareness regarding maintaining health records and its benefit in the effective management of clinical support. This service is for all citizens from anywhere and everywhere. Hc4U’s target is to bring citizenry into a smart treatment systems based on electronic records of health status to ensure treatment without delay in a cost effective manner.
                        </p>
                        <br>
                        <strong style="margin-left: 300px;"><q>Health is a right, not a privilege. It needs to be enjoyed with equity.</q></strong>
                        <br><br><br>
                        <h4>It is Important to maintain a Personal health Record (PHR) because;</h4>
                        <ul style="margin-left: 40px;">
                            <li>Wellness depends on management of healthcare</li>
                            <li>Healthcare records help
                                <ul style="margin-left: 50px;">
                                    <li>monitor health condition,</li>
                                    <li>coordinate care, treatment and</li>
                                    <li>ensure well being to prevent illness and hospitalization</li>
                                </ul>
                            </li>
                            <li>The Quality of treatment depends on Accessibility, Quality and Accuracy of healthcare records-past medical reports, medication</li>
                            <li>Healthcare information comes from many sources, like: Primary Care Doctors, Labs, Hospitals, and Self Home Care Diagnostic Equipment. Remains in different files unorganized.</li>
                            <li>Sharing Personal Health Record enables the Doctor to diagnose correctly based past medical history</li>
                            <li>Reduces unnecessary tests/procedures, thus saving time, money and finally helps reduce Medical Errors.</li>
                        </ul>

                        <h4>Problems that can be generated for not maintaining a Personal health Record (PHR) because;</h4>
                        <ul style="margin-left: 40px;">
                            <li>Medical records are generated from treatment of illness
                                <ul style="margin-left: 50px;">
                                    <li>Individual patients are responsible for maintaining records.</li>
                                    <li>Treatment starts from darkness in most cases.</li>
                                    <li>Treatment gets delayed due to lack of diagnostic reports on-site.</li>
                                    <li>New medical records are created every time for every individual, 	but get lost with the end of treatment.</li>
                                </ul>
                            </li>
                            <li>So, health-the most valuable asset, remain in darkness due to
                                <ul>
                                    <li>Systemic failure of maintaining comprehensive medical records.</li>
                                    <li>Prevention is totally ignored due to lack of awareness of the benefits of maintaining personal health records (PHR).</li>
                                </ul>
                            </li>
                        </ul>
                        <br>

                        <h4>Please find HC4U features and service detail below:</h4>
                        <ol type="1" style="margin-left: 40px;">
                            <li>e-PHR</li>
                            <li>Book Appointment</li>
                            <li>Discounts at Hospitals and Diagnostic Centers</li>
                            <li>Tele-medicine</li>
                            <li>View and Share Report</li>
                            <li>Health Insurance Facilities</li>
                            <li>Life Insurance Facilities</li>
                            <li>App Tour</li>
                            <li>Health Tips</li>
                            <li>E-Health Magazine</li>
                            <li>E-Health Library</li>
                        </ol>
                    </div>
                </div>
                <?php
                require_once 'db.php';
                $sql1 = "SELECT * FROM contact";
                $result2 = mysqli_query($link, $sql1);
                while ($row1 = mysqli_fetch_assoc($result2)) {
                    $data7[] = $row1;
                }
                ?>
                <div class="row" id="contact">
                    <?php
                    if (!empty($data7)) {
                    foreach ($data7 as $item1) {
                    ?>
                    <h3>Contact Us</h3>
                    <p style="margin-left: 30px;">
                        <?php echo $item1['description'];?>
                    </p>
                    <?php }} ?>
                </div>
                <div class="row">
                    <div id="map" style="margin-left: 30px;">

                    </div>
                </div>
                <br><br>

                <div class="row" id="condition">


                    <h3>Terms and Conditions</h3>
                    <?php
                    require_once 'db.php';
                    $sql1 = "SELECT * FROM condition";
                    $result2 = mysqli_query($link, $sql1);
                    while ($row1 = mysqli_fetch_assoc($result2)) {
                        $data8[] = $row1;
                    }
                    if (!empty($data8)) {
                    foreach ($data8 as $item1) {
                    ?>
                    <p style="margin-left: 30px;">
                        <?php echo $item1['description'];?>
                    </p><br>

                    <?php }} ?>
                </div>
                               </div>
        </section>

        <div id="achieve">
            <div class="container">
                <div class="row">
                    <br>
                    <div class="col-sm-5 col-md-5 form-inline text-center">
                        <label> News Letter:</label>
                        <input type="text" name="" placeholder="  Please enter your Email Address for Newsletter" style="border-radius: 10px; width: 330px; height: 30px; color: black">
                    </div>

                    <div class="col-sm-3 col-md-3 text-center">
                        <div class="row text-center">
                            <a href="https://itunes.apple.com/gb/app/hc4u/id1038842422?mt=8" target="_blank" style="text-decoration: none;">
                                <img src="img/ios.png">
                            </a>
                            <a href="https://play.google.com/store/apps/details?id=com.healthcare.hc4u&hl=en" target="_blank" style="text-decoration: none;">
                                <img src="img/android.png">
                            </a>
                        </div>
                    </div>

                    <div class="col-sm-3 col-md-3 form-inline text-center">
                        <label>Social Links: </label>&nbsp;&nbsp;
                        <a href="https://twitter.com/Hc4ubd" target="_blank" style="text-decoration: none;">
                            <span><i class="fa fa-twitter-square fa-3x" aria-hidden="true"></i></span>
                            <!--<img src="img/fb.png">-->
                        </a>&nbsp;&nbsp;
                        <a href="https://www.facebook.com/hc4ubd" target="_blank" style="text-decoration: none;">
                            <span><i class="fa fa-facebook-square fa-3x" aria-hidden="true"></i></span>
                            <!--<img src="img/twitter.png">-->
                        </a>
                    </div>
                </div><br>
            </div>
        </div>

        <footer>
            <div class="container">
                <div class="row text-left">
                    <div class="col-md-4">
                        <h3>Health Care</h3>
                        <ul>
                            <li><a href="http://localhost/magazine" style="text-decoration: none">eHealth Magazine</a></li>
                            <li><a href="http://localhost/library" style="text-decoration: none">eHealth Library</a></li>
                        </ul>
                    </div>
                    <div class="col-md-4">
                        <h3>Information</h3>
                        <ul>
                            <li><a href="aboutus.php#about" style="text-decoration: none">About Us</a></li>
                            <li><a href="aboutus.php#contact" style="text-decoration: none">Contact Us</a></li>
                            <li><a href="aboutus.php#condition" style="text-decoration: none">Terms and Conditions</a></li>
                        </ul>
                    </div>
                    <div class="col-md-4">
                        <h3>My Account</h3>
                        <ul>
                            <li><a href="tools.php" style="text-decoration: none">My Benefits</a></li>
                            <li><a href="http://healthcare4umember.com/HC4U/login" style="text-decoration: none">Member E-Card</a></li>
                            <li><a href="http://healthcare4umember.com/HC4U/login" style="text-decoration: none">My Insurance</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </footer>

        <script>var map;
            function initMap() {
                var myLatLng = {lat: 23.790237, lng: 90.402381};
                var map = new google.maps.Map(document.getElementById('map'), {
                    zoom: 16,
                    center: myLatLng
                });
                var marker = new google.maps.Marker({
                    position: myLatLng,
                    map: map
                });
            }
        </script>
        <script src="https://maps.googleapis.com/maps/api/js?callback=initMap" async defer></script>

        <script src="js/jquery-1.11.1.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/slick.min.js"></script>
        <script src="js/placeholdem.min.js"></script>
        <script src="js/rs-plugin/js/jquery.themepunch.plugins.min.js"></script>
        <script src="js/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
        <script src="js/waypoints.min.js"></script>

        <script src="js/scripts.js"></script>
        <script>
            $(document).ready(function() {
                appMaster.preLoader();
            });
        </script>
    </body>
</html>
