<?php

require_once "header.php";

?>
<div class="wrapper">

    <section id="partners">
        <div class="container">
            <h3>Partners</h3>
            <hr>
            <?php
            $sql = "SELECT * FROM corporation WHERE status=1";
            $result3 = mysqli_query($link, $sql);
            while ($row = mysqli_fetch_assoc($result3)) {
                $data4[] = $row;
            }
            ?>
            <div class="row" id="teletalk">
                <?php
                if (!empty($data4)) {
                foreach ($data4 as $item4) {
                ?>
                <div class="col-md-12">
                    <img src="admin/public/<?php echo $item4['image'];?>" height="250" width="450">

                    <h4><strong><?php echo  strip_tags($item4['title']);?></strong></h4>
                    <p>
                        Teletalk' (Bengali: টেলিটক) is the only state-run GSM and 3G based mobile phone operator in Bangladesh that started operating in 2004.
                        It is the fifth largest mobile operator in Bangladesh with a subscriber base of 4.211 million.
                        Teletalk has 10 MHz 3G spectrum of 2100 MHz Band. Teletalk offers both prepaid and postpaid plans for its 3G and 2G users.
                        It is renowned for offering special prepaid plans 'Agami', 'Bornomala' and 'Youth'.
                        The operator also offers 3G broadband devices such as Flash MODEM, Flash Router and MiFi Router.
                    </p>
                    <p>
                        <strong>Teletalk Bangladesh Ltd.</strong><br>
                        House no# 39, Rd no# 116,<br>
                        Gulshan-1, Dhaka-1212<br>
                        Bangladesh<br>
                        Tel: +88 02 9851056<br>
                        Fax: +88 02 9882828<br>
                        Web Site: <a href="http://www.teletalk.com.bd">www.teletalk.com.bd</a>
                    </p>
                </div>
            </div><br><br>
            <?php }} ?>

<!--            <div class="row" id="infomed">-->
<!--                <div class="col-md-12">-->
<!--                    <img src="admin/public/--><?php //echo $item4['image'];?><!--" height="200" width="450">-->
<!--                    --><?php //}} ?>
<!--                    <h4><strong>InfoMed, Malaysia’s Premier Healthcare Magazine</strong></h4>-->
<!--                    <p>-->
<!--                        We strongly believe that healthcare should be both personal and professional responsibility.-->
<!--                        We make it our mission to provide engaging and diversified coverage on government policies & programs in Malaysia and around the world, as well as the latest clinical studies & research findings, and of course, personal health.-->
<!--                        Our extensive reach includes doctors, healthcare professionals and the public.-->
<!--                    </p>-->
<!--                    <p>-->
<!--                        Web Site: <a href="http://infomed.com.my/">http://infomed.com.my/</a>-->
<!--                    </p>-->
<!--                </div>-->
<!--            </div><br><br>-->
<!---->
<!--            <div class="row" id="pragati">-->
<!--                <div class="col-md-12">-->
<!--                    <img src="img/partners/pragati_life.jpg" height="100" width="300">-->
<!--                    <h4><strong>Pragati Life Insurance Ltd.</strong></h4>-->
<!--                    <p>-->
<!--                        Pragati Insurance Ltd. is a leading private non-life insurance company in Bangladesh.-->
<!--                        It was established in 1986 by a group of young Bangladeshi entrepreneurs who had earlier launched a Commercial Bank in the private sector also.-->
<!--                        These Sponsors included Shipping Magnates, Engineers, Road Builders and Top Garment Industrialists.-->
<!--                        The company offers a complete range of general insurance products and services in motor, marine, energy, property and casualty, health, accident and liability areas.-->
<!--                    </p>-->
<!--                    <p>-->
<!--                        <strong>Pragati Life Insurance Ltd.</strong><br>-->
<!--                        83 Bir Uttam Samsul Alam Sarak,<br>-->
<!--                        Dhaka 1217, Bangladesh<br>-->
<!--                        Phone:+88 02-8321844<br>-->
<!--                        Web Site: <a href="http://www.pragatiinsurance.com/">http://www.pragatiinsurance.com/</a><br><br>-->
<!--                    </p>-->
<!--                </div>-->
<!--            </div><br><br>-->
<!---->
<!--            <div class="row" id="robi">-->
<!--                <div class="col-md-12">-->
<!--                    <img src="img/partners/robi_logo.png" height="100" width="400">-->
<!--                    <h4><strong>Robi Axiata Limited</strong></h4>-->
<!--                    <p>-->
<!--                        Robi (Bengali: রবি), formerly known as AKTel, is a joint venture between Axiata Group Berhad, Malaysia and NTT DoCoMo Inc, Japan.It provides 3.5G and 2G services.-->
<!--                        Robi is the third largest mobile phone operator in Bangladesh with 27.553 million subscribers as of February 2016.-->
<!--                    </p>-->
<!--                    <p>-->
<!--                        Robi boasts of the widest international roaming service in the market, connecting over 500 operators across 207 countries.-->
<!--                        It is the first operator in the country to introduce GPRS.-->
<!--                        Robi uses GSM 900/1800 MHz standard and operates 2G services on allocated 12.8 MHz frequency spectrum.[2] For 3.5G, it has 5 MHz spectrum on 2100 MHz band.-->
<!--                    </p>-->
<!--                    <p>-->
<!--                        <strong>Robi Axiata Limited</strong><br>-->
<!--                        Robi Corporate Office, <br>-->
<!--                        53 Gulshan South Avenue,<br>-->
<!--                        Gulshan-1, Dhaka, Bangladesh<br>-->
<!--                        Web Site: <a href="http://www.robi.com.bd">http://www.robi.com.bd</a>-->
<!--                    </p>-->
<!--                </div>-->
<!--            </div><br><br>-->
<!--            <div class="row" id="dghs">-->
<!--                <div class="col-md-12">-->
<!--                    <img src="img/partners/dghs.png" height="200" width="800">-->
<!--                    <h4><strong>Directorate General Health Service (DGHS)</strong></h4>-->
<!--                    <p>-->
<!--                        DGHS falls under the Government of People’s Republic Bangladesh, Ministry of Health and Family Welfare.-->
<!--                        They have taken many initiatives to make a positive impact in every citizen’s life in Bangladesh.-->
<!--                        They have been working on their goals to achieve their plan of making Bangladesh a better place to live.-->
<!--                        Thus HC4U is more than obliged to be a part of such a noble plan.-->
<!--                    </p>-->
<!--                    <p>-->
<!--                        <strong>Directorate General of Health Services</strong><br>-->
<!--                        Mohakhali, Dhaka 1212<br>-->
<!--                        Tel: 02-8816412, 8816459;<br>-->
<!--                        Fax: 02-8813875<br>-->
<!--                        Email: <a href="#">info@dghs.gov.bd</a><br>-->
<!--                        Web Site: <a href="http://www.dghs.gov.bd">www.dghs.gov.bd,</a>-->
<!--                    </p>-->
<!--                </div>-->
<!--            </div><br><br>-->
        </div><br><br>

    </section>

    <div id="achieve">
        <div class="container">
            <br>
            <div class="row" >
                <div class="col-sm-5 col-md-5 form-inline text-center">
                    <label> News Letter:</label>
                    <input type="text" name="" placeholder="  Please enter your Email Address for Newsletter" style="border-radius: 10px; width: 330px; height: 30px; color: black;">
                </div>

                <div class="col-sm-3 col-md-3 text-center">
                    <div class="row text-center">
                        <a href="https://itunes.apple.com/gb/app/hc4u/id1038842422?mt=8" target="_blank" style="text-decoration: none;">
                            <img src="img/ios.png">
                        </a>
                        <a href="https://play.google.com/store/apps/details?id=com.healthcare.hc4u&hl=en" target="_blank" style="text-decoration: none;">
                            <img src="img/android.png">
                        </a>
                    </div>
                </div>

                <div class="col-sm-3 col-md-3 form-inline text-center">
                    <!--<label>Social Links: </label>&nbsp;&nbsp;-->
                    <strong>Social Links:</strong>
                    <a href="https://twitter.com/Hc4ubd" target="_blank" style="text-decoration: none;">
                        <span><i class="fa fa-twitter-square fa-3x" aria-hidden="true"></i></span>
                        <!--<img src="img/fb.png">-->
                    </a>&nbsp;&nbsp;
                    <a href="https://www.facebook.com/hc4ubd" target="_blank" style="text-decoration: none;">
                        <span><i class="fa fa-facebook-square fa-3x" aria-hidden="true"></i></span>
                        <!--<img src="img/twitter.png">-->
                    </a>
                </div>
            </div><br>
        </div>
    </div>

    <?php require_once "footer.php"; ?>
    
</div>


<script src="js/jquery-1.11.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/slick.min.js"></script>
<script src="js/placeholdem.min.js"></script>
<script src="js/rs-plugin/js/jquery.themepunch.plugins.min.js"></script>
<script src="js/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
<script src="js/waypoints.min.js"></script>
<script src="js/scripts.js"></script>
<script>
    $(document).ready(function() {
        appMaster.preLoader();
    });
</script>
</body>

</html>
