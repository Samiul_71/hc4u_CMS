<?php require_once "header.php"; ?>

<div class="wrapper">

    <section id="products">
        <div class="container">
            <div class="row">
                <?php
                $sql = "SELECT * FROM others_products WHERE status=1";
                $result3 = mysqli_query($link, $sql);
                while ($row = mysqli_fetch_assoc($result3)) {
                    $data4[] = $row;
                }
                ?>
                <div class="media">
                    <div class="media-left">
                        <?php
                        if (!empty($data4)) {
                        foreach ($data4 as $item4) {
                        ?>
                        <a href="#">
                            <img class="media-object" src="admin/public/<?php echo $item4['image'];?>" height="100px" width="100px">
                        </a>

                        <?php }} ?></div>
                    <br>
                    <div class="media-body">
                        HC4U is a cloud based platform that collects and records personal health information of users through Doctors, Labs, Hospitals, Diagnostic Equipment and makes it available to the users or user authorized doctors and hospitals anytime through mobile or web apps.
                        Provides real-time access to Personal Health Records (PHR), enabling Hospitals and Doctors to diagnose patient quickly based on past history, saving time and hassle.
                        HC4U helps coordinate care & treatment, and prevent illness and hospitalization
                        Become our member today, please click the following links for further information:<br>
                        For subscribed members: <a href="http://healthcare4umember.com/HC4U/login">http://healthcare4umember.com/HC4U/login</a>
                        <br>
                        For subscription: <a href="http://healthcare4umember.com/HC4U/signup">http://healthcare4umember.com/HC4U/signup</a>
                    </div>
                </div>

            </div>
            <br><br>
            <!--            <div class="row">-->
            <!--                <div class="media">-->
            <!--                    <div class="media-left">-->
            <!--                        <a href="#">-->
            <!--                            <img class="media-object" src="img/tele_daktar.png" height="90px" width="220px">-->
            <!--                        </a>-->
            <!--                    </div><br>-->
            <!--                    <div class="media-body">-->
            <!--                        TeleDaktar is a cloud based IT application that collects and records personal health information of users through Doctors, Labs, Hospitals, Diagnostic Equipment and makes it available to the users or user authorized doctors and hospitals anytime through mobile or web apps.-->
            <!--                        Provides real-time access to Personal Health Records (PHR), enabling Hospitals and Doctors to diagnose patient quickly based on past history, saving time and hassle.-->
            <!--                        HC4U helps coordinate care & treatment, and prevent illness and hospitalization-->
            <!--                    </div>-->
            <!--                </div>-->
            <!--                <br><br>-->
            <!--            </div>-->

        </div>
    </section>

    <div id="achieve">
        <div class="container">
            <br>
            <div class="row">
                <div class="col-sm-5 col-md-5 form-inline text-center">
                    <label> News Letter:</label>
                    <input type="text" name="" placeholder="  Please enter your Email Address for Newsletter" style="border-radius: 10px; width: 330px; height: 30px; color: black;">
                </div>

                <div class="col-sm-3 col-md-3 text-center">
                    <div class="row text-center">
                        <a href="https://itunes.apple.com/gb/app/hc4u/id1038842422?mt=8" target="_blank" style="text-decoration: none;">
                            <img src="img/ios.png">
                        </a>
                        <a href="https://play.google.com/store/apps/details?id=com.healthcare.hc4u&hl=en" target="_blank" style="text-decoration: none;">
                            <img src="img/android.png">
                        </a>
                    </div>
                </div>

                <div class="col-sm-3 col-md-3 form-inline text-center">
                    <label>Social Links: </label>&nbsp;&nbsp;
                    <a href="https://twitter.com/Hc4ubd" target="_blank" style="text-decoration: none;">
                        <span><i class="fa fa-twitter-square fa-3x" aria-hidden="true"></i></span>
                        <!--<img src="img/fb.png">-->
                    </a>&nbsp;&nbsp;
                    <a href="https://www.facebook.com/hc4ubd" target="_blank" style="text-decoration: none;">
                        <span><i class="fa fa-facebook-square fa-3x" aria-hidden="true"></i></span>
                        <!--<img src="img/twitter.png">-->
                    </a>
                </div>
            </div><br>
        </div>
    </div>

</div>

<?php require_once "footer.php"; ?>

<script src="js/jquery-1.11.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/slick.min.js"></script>
<script src="js/placeholdem.min.js"></script>
<script src="js/rs-plugin/js/jquery.themepunch.plugins.min.js"></script>
<script src="js/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
<script src="js/waypoints.min.js"></script>
<script src="js/scripts.js"></script>
<script>
    $(document).ready(function() {
        appMaster.preLoader();
    });
</script>
</body>

</html>
