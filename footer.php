<footer>
    <div class="container">
        <div class="row text-left">
            <div class="col-md-4">
                <h3>Health Care</h3>
                <ul>
                    <li><a href="magazine.php" style="text-decoration: none">eHealth Magazine</a></li>
                    <li><a href="library.php" style="text-decoration: none">eHealth Library</a></li>
                </ul>
            </div>
            <div class="col-md-4">
                <h3>Information</h3>
                <ul>
                    <li><a href="aboutus.php" style="text-decoration: none">About Us</a></li>
                    <li><a href="aboutus.php#contact" style="text-decoration: none">Contact Us</a></li>
                    <li><a href="aboutus.php#condition" style="text-decoration: none">Terms and Conditions</a></li>
                </ul>
            </div>
            <div class="col-md-4">
                <h3>My Account</h3>
                <ul>
                    <li><a href="tools.php" style="text-decoration: none">My Benefits</a></li>
                    <li><a href="http://healthcare4umember.com/HC4U/login" style="text-decoration: none">Member E-Card</a></li>
                    <li><a href="http://healthcare4umember.com/HC4U/login" style="text-decoration: none">My Insurance</a></li>
                </ul>
            </div>
        </div>
    </div>
</footer>
