<?php
require_once 'header.php';

?>

<div class="wrapper">
    <section id="events">
        <div class="container">
            <h3 id="csr">CSR</h3>
            <hr>
            <?php
            $sql = "SELECT * FROM csr WHERE status=1";
            $result1 = mysqli_query($link, $sql);
            while ($row1 = mysqli_fetch_assoc($result1)) {
                $data1[] = $row1;
            }
            ?>
            <div class="row">
                <?php
                if (!empty($data1)) {
                foreach ($data1 as $item1) {
                ?>
                <p><strong><?php echo  $item1['title'];?></strong></p>
                <div class="col-md-5">
                    <img src="admin/public/<?php echo $item1['image'];?>" style="height: 420px; width: 300px;">
                </div>
                <div class="col-md-7">
                    <img src="admin/public/<?php echo $item1['image1'];?>" style="height: 200px; width: 500px;"><br><br>
                    <img src="admin/public/<?php echo $item1['image2'];?>" style="height: 200px; width: 500px;">
                </div>
                    <br><br>
                <?php } } ?>
            </div><br><br>
<!--            <div class="row">-->
<!--                <div>-->
<!--                    <p><strong>Prime Bank International English medium School</strong></p>-->
<!--                    <p>Prime Bank International English medium School is located at Uttara sector 12.<br>-->
<!--                        This school is a concern of Prime Bank who has adopted a new method of teaching children their regular lesson in a more interactive way.-->
<!--                    </p>-->
<!--                </div>-->
<!--            </div>-->
<!--            <div class="row">-->
<!--                <div class="col-md-12 col-sm-12">-->
<!--                    <img src="img/events/event_prime_bank1.jpg" style="height: 300px; width: 250px;">&nbsp;&nbsp;&nbsp;&nbsp;-->
<!--                    <img src="img/events/event_prime_bank2.jpg" style="height: 300px; width: 400px;">-->
<!--                </div>-->
<!--            </div>-->
            <br><br><br><br>
            <h3 id="fair">Fair</h3>
            <hr>
            <?php
            $sql = "SELECT * FROM fair WHERE status=1";
            $result1 = mysqli_query($link, $sql);
            while ($row1 = mysqli_fetch_assoc($result1)) {
                $data2[] = $row1;
            }
            ?>
            <div class="row">

                <div>
                    <?php
                    if (!empty($data2)) {
                    foreach ($data2 as $item1) {
                    ?>
                    <p><strong><?php echo  $item1['title'];?></strong></p>
<!--                    <p>-->
<!--                        Malaysian International Health Fair (2015) took place at Sheraton Hotel during September 2015.<br>-->
<!--                        This was the 1st health fair conducted by the Malaysian entities.<br>-->
<!--                        Malaysian Health Minister (as you can see below) was also present during this event.-->
<!--                    </p>-->
                </div>
            </div>
            <div class="row">

                        <div class="col-md-5">
                            <img src="admin/public/<?php echo $item1['image'];?>" style="height: 420px; width: 300px;">
                        </div>
                        <div class="col-md-7">
                            <img src="admin/public/<?php echo $item1['image1'];?>" style="height: 200px; width: 500px;"><br><br>
                            <img src="admin/public/<?php echo $item1['image2'];?>" style="height: 200px; width: 500px;">
                        </div>
                        <br><br>
                    <?php } } ?>
            </div><br><br>

            <br><br><br><br>
            <h3 id="competition">Competitions</h3>
            <hr>
            <?php
            $sql = "SELECT * FROM competition WHERE status=1";
            $result1 = mysqli_query($link, $sql);
            while ($row1 = mysqli_fetch_assoc($result1)) {
                $data3[] = $row1;
            }
            ?>
            <div class="row">
                <div>
                    <?php
                    if (!empty($data3)) {
                    foreach ($data3 as $item1) {
                    ?>
                    <p><strong><?php echo  $item1['title'];?></strong></p>
                    <p>
                        GrameenPhone Accelerator, along with SD Asia hosted an event in January 2016 for start-ups in Bangladesh.<br>
                        Many companies competed and finally 6 companies were announced the winner. HC4U won the competition along with 5 other start-ups.
                    </p>
                </div>
            </div>
            <div class="row">


                        <div class="col-md-5">
                            <img src="admin/public/<?php echo $item1['image'];?>" style="height: 420px; width: 300px;">
                        </div>
                        <div class="col-md-7">
                            <img src="admin/public/<?php echo $item1['image1'];?>" style="height: 200px; width: 500px;"><br><br>
                            <img src="admin/public/<?php echo $item1['image2'];?>" style="height: 200px; width: 500px;"><br><br>
                        </div>
                        <br><br>
                    <?php } } ?>
            </div><br><br>

            <br><br><br><br>
            <h3 id="upcoming_events">Up Coming Events...</h3>
            <hr>
            <?php
            $sql = "SELECT * FROM upcoming_event WHERE status=1";
            $result1 = mysqli_query($link, $sql);
            while ($row1 = mysqli_fetch_assoc($result1)) {
                $data4[] = $row1;
            }
            ?>
            <div class="row">
                <div>
                    <?php
                    if (!empty($data4)) {
                    foreach ($data4 as $item1) {
                    ?>
                    <p><strong></strong></p>
                    <p>
                        <img src="admin/public/<?php echo $item1['image'];?>" style="height: 50px; width: 80px;">
                        <?php echo $item1['description'];?>
<!--                        HC4U is taking part in E27, Echelon Asia Summit 2016, where HC4U is currently holding position number 9 out of 100 in this event.<br><br>-->
<!--                        International Health Fair – HC4U is intending to host an International Health Fair for 3 continuous days in November 2016-->
                    </p><br><br>
                    <?php }} ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 col-sm-12">

                </div>
            </div>
        </div>
    </section>
</div>
<div id="achieve">
    <div class="container">
        <div class="row">
            <br>
            <div class="col-sm-5 col-md-5 form-inline text-center">
                <label> News Letter:</label>
                <input type="text" name="" placeholder="  Please enter your Email Address for Newsletter" style="border-radius: 10px; width: 330px; height: 30px; color: black">
            </div>

            <div class="col-sm-3 col-md-3 text-center">
                <div class="row text-center">
                    <a href="https://itunes.apple.com/gb/app/hc4u/id1038842422?mt=8" target="_blank" style="text-decoration: none;">
                        <img src="img/ios.png">
                    </a>
                    <a href="https://play.google.com/store/apps/details?id=com.healthcare.hc4u&hl=en" target="_blank" style="text-decoration: none;">
                        <img src="img/android.png">
                    </a>
                </div>
            </div>

            <div class="col-sm-3 col-md-3 form-inline text-center">
                <label>Social Links: </label>&nbsp;&nbsp;
                <a href="https://twitter.com/Hc4ubd" target="_blank" style="text-decoration: none;">
                    <span><i class="fa fa-twitter-square fa-3x" aria-hidden="true"></i></span>
                    <!--<img src="img/fb.png">-->
                </a>&nbsp;&nbsp;
                <a href="https://www.facebook.com/hc4ubd" target="_blank" style="text-decoration: none;">
                    <span><i class="fa fa-facebook-square fa-3x" aria-hidden="true"></i></span>
                    <!--<img src="img/twitter.png">-->
                </a>
            </div>
        </div><br>
    </div>
</div>

<?php require_once "footer.php"; ?>

<script src="js/jquery-1.11.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/slick.min.js"></script>
<script src="js/placeholdem.min.js"></script>
<script src="js/rs-plugin/js/jquery.themepunch.plugins.min.js"></script>
<script src="js/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
<script src="js/waypoints.min.js"></script>
<script src="js/scripts.js"></script>
<script>
    $(document).ready(function() {
        appMaster.preLoader();
    });
</script>
</body>
</html>
