<?php


//Route::get('/', 'AdminController@index');
Route::get('/', 'HomeController@index');

Route::auth();

Route::get('/home', 'HomeController@index');


Route::resource('logo', 'LogoController');

Route::resource('tools', 'ToolsController');

Route::resource('about', 'AboutController');

Route::resource('slider', 'SliderController');
Route::post('slider/delete', 'SliderController@deleteSlider');

Route::resource('gallary', 'GallaryController');

Route::resource('hospital', 'HospitalController');

Route::resource('corporation', 'CorporationController');

Route::resource('main_product', 'Main_productController');

Route::resource('other_product', 'Other_productController');

Route::resource('csr', 'CsrController');

Route::resource('fair', 'FairController');

Route::resource('competition', 'CompetitionController');

Route::resource('upcoming_event', 'Upcoming_eventController');

Route::resource('emagazine', 'EmagazineController');

Route::resource('elibrary', 'ElibraryController');

Route::resource('achivement', 'AchivementController');

Route::resource('establishment', 'EstablishmentController');

Route::resource('publication', 'PublicationController');

Route::resource('contact', 'ContactController');

Route::resource('condition', 'ConditionController');




