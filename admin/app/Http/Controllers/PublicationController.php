<?php

namespace App\Http\Controllers;


namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class PublicationController extends Controller
{
    public  function index(){
        $publication= DB::table('publication')->get();
        return view('publication.index', compact('publication', $publication));
    }
    public function create(){
        return view('publication.create');
    }
    public function store(){
        $input = Input::all();
        $file = array_get($input,'image');
        $destinationPath = 'uploads/publication';
        $extension = $file->getClientOriginalExtension();
        $fileName = rand(11111, 99999) . '.' . $extension;
        $file_path = $file->move($destinationPath, $fileName);

        DB::table('publication')->insert(array(
            'title' => trim($input['title']),
            'sub_title' => trim($input['sub_title']),
            'description' => trim($input['description']),
            'image' => $file_path,
            'status' =>'1'
        ));
        return redirect('publication');

    }
    public function edit($id){
        $publication = DB::table('publication')->where('id', $id)->first();
//        dd($tools);
        return view('publication.edit', compact('publication', $publication));

    }
    public function update($id){
        $input = Input::all();
        $file = array_get($input,'image');
        $destinationPath = 'uploads/publication';
        $extension = $file->getClientOriginalExtension();
        $fileName = rand(11111, 99999) . '.' . $extension;
        $file_path = $file->move($destinationPath, $fileName);


        DB::table('publication')
            ->where('id', $id)
            ->update(array(
                'title' => trim($input['title']),
                'sub_title' => trim($input['sub_title']),
                'description' => trim($input['description']),
                'image' => $file_path,
                'status' =>'1'
            ));
        return redirect('publication');
    }
    public function show($id){

    }
    public function destroy($id){
        $slider = DB::table('slider')->where('id',$id)->delete();
        if($slider){
            return redirect('slider');
        } else{
            echo "Failed to Delete";
        }
    }

    public function deleteSlider($id){

        print_r($id);

    }

}
