<?php

namespace App\Http\Controllers;


namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class AboutController extends Controller
{
    public  function index(){
        $about= DB::table('about_us')->get();
        return view('about.index', compact('about', $about));
    }
    public function create(){
        return view('about.create');
    }
    public function store(){
        $input = Input::all();
        $file = array_get($input,'image');
        $destinationPath = 'uploads/about';
        $extension = $file->getClientOriginalExtension();
        $fileName = rand(11111, 99999) . '.' . $extension;
        $file_path = $file->move($destinationPath, $fileName);

        DB::table('about_us')->insert(array(
            'title' => trim($input['title']),
            'image' => $file_path,
            'content' => trim($input['content']),
            'status' =>'1'
        ));
        return redirect('about');

    }
    public function edit($id){
//        dd($id);
        $about = DB::table('about_us')->where('id', $id)->first();
//        dd($tools);
        return view('about.edit', compact('about', $about));
    }
    public function update($id){
        $input = Input::all();
        $file = array_get($input,'image');
        $destinationPath = 'uploads/about';
        $extension = $file->getClientOriginalExtension();
        $fileName = rand(11111, 99999) . '.' . $extension;
        $file_path = $file->move($destinationPath, $fileName);


        DB::table('about_us')
            ->where('id', $id)
            ->update(array(
                'title' => trim($input['title']),
                'image' => $file_path,
                'content' => trim($input['content']),
                'status' =>'1'
            ));
        return redirect('about');

    }
    public function show($id){

    }
    public function destroy($id){

    }
}
