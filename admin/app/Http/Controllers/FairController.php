<?php

namespace App\Http\Controllers;


namespace App\Http\Controllers;

use App\Fair;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class FairController extends Controller
{
    public  function index(){
        $fair= DB::table('fair')->get();
        return view('fair.index', compact('fair', $fair));
    }
    public function create(){
        return view('fair.create');
    }
    public function store(){
        $input = Input::all();
        $file = array_get($input,'image');
        $destinationPath = 'uploads/fair';
        $extension = $file->getClientOriginalExtension();
        $fileName = rand(11111, 999999) . '.' . $extension;
        $file_path = $file->move($destinationPath, $fileName);

        $file1 = array_get($input,'image1');
        $destinationPath1 = 'uploads/fair';
        $extension1 = $file1->getClientOriginalExtension();
        $fileName1 = rand(11111, 99999) . '.' . $extension1;
        $file_path1 = $file1->move($destinationPath1, $fileName1);

        $file2 = array_get($input,'image2');
        $destinationPath2 = 'uploads/fair';
        $extension2 = $file2->getClientOriginalExtension();
        $fileName2 = rand(11111, 99999) . '.' . $extension2;
        $file_path2 = $file2->move($destinationPath2, $fileName2);

        DB::table('fair')->insert(array(
            'title' => trim($input['title']),
            'image' => $file_path,
            'image1' => $file_path1,
            'image2' => $file_path2,
            'description' => trim($input['description']),
            'status' =>'1'
        ));
        return redirect('fair');

    }
    public function edit($id){
        $fair= DB::table('fair')->where('id', $id)->first();
//        dd($tools);
        return view('fair.edit', compact('fair', $fair));
    }
    public function update($id){
        $input = Input::all();
        $file = array_get($input,'image');
        $destinationPath = 'uploads/fair';
        $extension = $file->getClientOriginalExtension();
        $fileName = rand(11111, 99999) . '.' . $extension;
        $file_path = $file->move($destinationPath, $fileName);

        $file1 = array_get($input,'image1');
        $destinationPath1 = 'uploads/fair';
        $extension3 = $file1->getClientOriginalExtension();
        $fileName1 = rand(11111, 999999) . '.' . $extension3;
        $file_path1 = $file1->move($destinationPath1, $fileName1);

        $file2 = array_get($input,'image2');
//        dd($file2);
        $destinationPath2 = 'uploads/fair';
        $extension4 = $file2->getClientOriginalExtension();
        $fileName2 = rand(11111, 999999) . '.' . $extension4;
        $file_path2 = $file->move($destinationPath2, $fileName2);


        DB::table('fair')
            ->where('id', $id)
            ->update(array(
                'title' => trim($input['title']),
                'image' => $file_path,
                'image1' => $file_path1,
                'image2' => $file_path2,
                'description' => trim($input['description']),
                'status' =>'1'
            ));
        return redirect('fair');
    }
    public function show($id){

    }
    public function destroy($id){
        $fair = Fair::findOrFail($id);
//        dd($tools);
        $fair->delete();

//        Session::flash('flash_message', 'Task successfully deleted!');

        return redirect()->route('fair.index');
    }

    public function deleteSlider($id){

        print_r($id);

    }

}
