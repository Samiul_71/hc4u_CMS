<?php

namespace App\Http\Controllers;


namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class SliderController extends Controller
{
    public  function index(){
        $slider= DB::table('slider')->get();
        return view('slider.index', compact('slider', $slider));
    }
    public function create(){
        return view('slider.create');
    }
    public function store(){
        $input = Input::all();
        $file = array_get($input,'image');
        $destinationPath = 'uploads/slider';
        $extension = $file->getClientOriginalExtension();
        $fileName = rand(11111, 99999) . '.' . $extension;
        $file_path = $file->move($destinationPath, $fileName);

        DB::table('slider')->insert(array(
            'name' => trim($input['name']),
            'image' => $file_path,
            'description' => trim($input['description']),
            'status' =>'1'
        ));
        return redirect('slider');

    }
    public function edit($id){
        $slider = DB::table('slider')->where('id', $id)->first();
//        dd($tools);
        return view('slider.edit', compact('slider', $slider));
    }
    public function update($id){
        $input = Input::all();
        $file = array_get($input,'image');
        $destinationPath = 'uploads/slider';
        $extension = $file->getClientOriginalExtension();
        $fileName = rand(11111, 99999) . '.' . $extension;
        $file_path = $file->move($destinationPath, $fileName);


        DB::table('slider')
            ->where('id', $id)
            ->update(array(
                'name' => trim($input['name']),
                'image' => $file_path,
                'description' => trim($input['description']),
                'status' =>'1'
            ));
        return redirect('slider');
    }
    public function show($id){

    }
    public function destroy($id){
        $slider = DB::table('slider')->where('id',$id)->delete();
        if($slider){
            return redirect('slider');
        } else{
            echo "Failed to Delete";
        }
    }

    public function deleteSlider($id){

        print_r($id);

    }

}
