<?php

namespace App\Http\Controllers;


namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class AchivementController extends Controller
{
    public  function index(){
        $achivement= DB::table('achivement')->get();
        return view('achivement.index', compact('achivement', $achivement));
    }
    public function create(){
        return view('achivement.create');
    }
    public function store(){
        $input = Input::all();
        $file = array_get($input,'image');
        $destinationPath = 'uploads/achivement';
        $extension = $file->getClientOriginalExtension();
        $fileName = rand(11111, 99999) . '.' . $extension;
        $file_path = $file->move($destinationPath, $fileName);

        DB::table('achivement')->insert(array(
            'title' => trim($input['title']),
            'sub_title' => trim($input['sub_title']),
            'description' => trim($input['description']),
            'image' => $file_path,
            'status' =>'1'
        ));
        return redirect('achivement');

    }
    public function edit($id){
        $achivement = DB::table('achivement')->where('id', $id)->first();
//        dd($tools);
        return view('achivement.edit', compact('achivement', $achivement));
    }
    public function update($id){
        $input = Input::all();
        $file = array_get($input,'image');
        $destinationPath = 'uploads/achivement';
        $extension = $file->getClientOriginalExtension();
        $fileName = rand(11111, 99999) . '.' . $extension;
        $file_path = $file->move($destinationPath, $fileName);


        DB::table('achivement')
            ->where('id', $id)
            ->update(array(
                'title' => trim($input['title']),
                'sub_title' => trim($input['sub_title']),
                'description' => trim($input['description']),
                'image' => $file_path,
                'status' =>'1'
            ));
        return redirect('achivement');
    }
    public function show($id){

    }
    public function destroy($id){
        $slider = DB::table('slider')->where('id',$id)->delete();
        if($slider){
            return redirect('slider');
        } else{
            echo "Failed to Delete";
        }
    }

    public function deleteSlider($id){

        print_r($id);

    }

}
