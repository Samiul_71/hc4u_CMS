<?php

namespace App\Http\Controllers;


namespace App\Http\Controllers;

use App\Main_product;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class Main_productController extends Controller
{
    public  function index(){
        $main_product= DB::table('main_products')->get();
        return view('main_product.index', compact('main_product', $main_product));
    }
    public function create(){
        return view('main_product.create');
    }
    public function store(){
        $input = Input::all();
        $file = array_get($input,'image');
        $destinationPath = 'uploads/main_product';
        $extension = $file->getClientOriginalExtension();
        $fileName = rand(11111, 99999) . '.' . $extension;
        $file_path = $file->move($destinationPath, $fileName);

        DB::table('main_products')->insert(array(
            'title' => trim($input['title']),
            'image' => $file_path,
            'description' => trim($input['description']),
            'status' =>'1'
        ));
        return redirect('main_product');

    }
    public function edit($id){
        $main_product = DB::table('main_products')->where('id', $id)->first();
//        dd($tools);
        return view('main_product.edit', compact('main_product', $main_product));
    }
    public function update($id){
        $input = Input::all();
        $file = array_get($input,'image');
        $destinationPath = 'uploads/main_product';
        $extension = $file->getClientOriginalExtension();
        $fileName = rand(11111, 99999) . '.' . $extension;
        $file_path = $file->move($destinationPath, $fileName);


        DB::table('main_products')
            ->where('id', $id)
            ->update(array(
                'title' => trim($input['title']),
                'image' => $file_path,
                'description' => trim($input['description']),
                'status' =>'1'
            ));
        return redirect('main_product');
    }
    public function show($id){

    }
    public function destroy($id){
        $main_product = Main_product::findOrFail($id);
//        dd($tools);
        $main_product->delete();

//        Session::flash('flash_message', 'Task successfully deleted!');

        return redirect()->route('main_product.index');
    }

    public function deleteSlider($id){

        print_r($id);

    }

}
