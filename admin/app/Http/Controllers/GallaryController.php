<?php

namespace App\Http\Controllers;


namespace App\Http\Controllers;

use App\Gallary;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;

class GallaryController extends Controller
{
    public  function index(){
        $gallary= DB::table('gallary')->get();
        return view('gallary.index', compact('gallary', $gallary));
    }
    public function create(){
        return view('gallary.create');
    }
    public function store(){
        $input = Input::all();
        $file = array_get($input,'image');
        $destinationPath = 'uploads/gallary';
        $extension = $file->getClientOriginalExtension();
        $fileName = rand(11111, 99999) . '.' . $extension;
        $file_path = $file->move($destinationPath, $fileName);

        DB::table('gallary')->insert(array(
            'title' => trim($input['title']),
            'image' => $file_path,
            'content' => trim($input['content']),
            'status' =>'1'
        ));
        return redirect('gallary');

    }
    public function edit($id){
        $gallary = DB::table('gallary')->where('id', $id)->first();
//        dd($tools);
        return view('gallary.edit', compact('gallary', $gallary));

    }
    public function update($id){
        $input = Input::all();
        $file = array_get($input,'image');
        $destinationPath = 'uploads/gallary';
        $extension = $file->getClientOriginalExtension();
        $fileName = rand(11111, 99999) . '.' . $extension;
        $file_path = $file->move($destinationPath, $fileName);


        DB::table('gallary')
            ->where('id', $id)
            ->update(array(
                'title' => trim($input['title']),
                'image' => $file_path,
                'content' => trim($input['content']),
                'status' =>'1'
            ));
        return redirect('gallary');

    }
    public function show($id){

    }
    public function destroy($id){

        $gallary = Gallary::findOrFail($id);
//        dd($gallary);
        $gallary->delete();

        Session::flash('flash_message', 'Task successfully deleted!');

        return redirect()->route('gallary.index');
    }

    public function deleteSlider($id){

        print_r($id);

    }

}
