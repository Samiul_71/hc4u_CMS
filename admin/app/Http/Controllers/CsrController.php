<?php

namespace App\Http\Controllers;


namespace App\Http\Controllers;

use App\Csr;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class CsrController extends Controller
{
    public  function index(){
        $csr= DB::table('csr')->get();
        return view('csr.index', compact('csr', $csr));
    }
    public function create(){
        return view('csr.create');
    }
    public function store(){
        $input = Input::all();
//        dd($input);
        $file = array_get($input,'image');
        $destinationPath = 'uploads/csr';
        $extension = $file->getClientOriginalExtension();
        $fileName = rand(11111, 999999) . '.' . $extension;
        $file_path = $file->move($destinationPath, $fileName);

        $file1 = array_get($input,'image1');
        $destinationPath1 = 'uploads/csr';
        $extension1 = $file1->getClientOriginalExtension();
        $fileName1 = rand(11111, 999999) . '.' . $extension1;
        $file_path1 = $file1->move($destinationPath1, $fileName1);

        $file2 = array_get($input,'image2');
        $destinationPath2 = 'uploads/csr';
        $extension2 = $file2->getClientOriginalExtension();
        $fileName2 = rand(11111, 999999) . '.' . $extension2;
        $file_path2 = $file2->move($destinationPath2, $fileName2);

        DB::table('csr')->insert(array(
            'title' => trim($input['title']),
            'image' => $file_path,
            'image1' => $file_path1,
            'image2' => $file_path2,
            'description' => trim($input['description']),
            'status' =>'1'
        ));
        return redirect('csr');

    }
    public function edit($id){
        $csr = DB::table('csr')->where('id', $id)->first();
//        dd($tools);
        return view('csr.edit', compact('csr', $csr));
    }
    public function update($id){
        $input = Input::all();

        $file = array_get($input,'image');
        $destinationPath = 'uploads/csr';
        $extension = $file->getClientOriginalExtension();
        $fileName = rand(11111, 999999) . '.' . $extension;
        $file_path = $file->move($destinationPath, $fileName);


        $file1 = array_get($input,'image1');
        $destinationPath1 = 'uploads/csr';
        $extension3 = $file1->getClientOriginalExtension();
        $fileName1 = rand(11111, 999999) . '.' . $extension3;
        $file_path1 = $file1->move($destinationPath1, $fileName1);

        $file2 = array_get($input,'image2');
//        dd($file2);
        $destinationPath2 = 'uploads/csr';
        $extension4 = $file2->getClientOriginalExtension();
        $fileName2 = rand(11111, 999999) . '.' . $extension4;
        $file_path2 = $file->move($destinationPath2, $fileName2);


        DB::table('csr')
            ->where('id', $id)
            ->update(array(
                'title' => trim($input['title']),
                'image' => $file_path,
                'image1' => $file_path1,
                'image2' => $file_path2,
                'description' => trim($input['description']),
                'status' =>'1'
            ));
        return redirect('csr');
    }
    public function show($id){

    }
    public function destroy($id){
        $csr = Csr::findOrFail($id);
//        dd($tools);
        $csr->delete();

//        Session::flash('flash_message', 'Task successfully deleted!');

        return redirect()->route('csr.index');
    }

    public function deleteSlider($id){

        print_r($id);

    }

}
