<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class ContactController extends Controller
{
    public function index(){
        $contact = DB::table('contact')->orderBy('id', 'ASC')->get();
        return view('contact.index', compact('contact', $contact));
    }
    public function create(){
        return view('contact.create');
    }
    public function store(){
        $input = Input::all();
//        dd($input);
        DB::table('contact')->insert(array(
            'title' => trim($input['title']),
            'description' => trim($input['description'])
        ));
        return redirect('contact');
    }
    public function edit($id){
//        dd($id);
        $contact = DB::table('contact')->where('id', $id)->first();
//        dd($tools);
        return view('contact.edit', compact('contact', $contact));
    }
    public function update($id){

        $input = Input::all();
        DB::table('contact')
            ->where('id', $id)
            ->update(array(
                'title' => trim($input['title']),
                'description' => trim($input['description'])
            ));
        return redirect('contact');
    }
    public function show($id){
      dd($id);
        $tools = DB::table('tools')->where('id', $id)->first();
//        dd($tools);
        
    }
    public function destroy($id){

    }
}
