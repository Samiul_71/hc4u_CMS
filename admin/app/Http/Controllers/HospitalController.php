<?php

namespace App\Http\Controllers;


namespace App\Http\Controllers;

use App\Hospital;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class HospitalController extends Controller
{
    public  function index(){
        $hospital= DB::table('hospital')->get();
        return view('hospital.index', compact('hospital', $hospital));
    }
    public function create(){
        return view('hospital.create');
    }
    public function store(){
        $input = Input::all();
        $file = array_get($input,'image');
        $destinationPath = 'uploads/hospital';
        $extension = $file->getClientOriginalExtension();
        $fileName = rand(11111, 99999) . '.' . $extension;
        $file_path = $file->move($destinationPath, $fileName);

        DB::table('hospital')->insert(array(
            'name' => trim($input['name']),
            'address' => trim($input['address']),
            'city' => trim($input['city']),
            'district' => trim($input['district']),
            'image' => $file_path,
            'description' => trim($input['description']),
            'discount' => trim($input['discount']),
            'status' =>'1'
        ));
        return redirect('hospital');

    }
    public function edit($id){
        $hospital = DB::table('hospital')->where('id', $id)->first();
//        dd($tools);
        return view('hospital.edit', compact('hospital', $hospital));

    }
    public function update($id){
        $input = Input::all();
        $file = array_get($input,'image');
        $destinationPath = 'uploads/hospital';
        $extension = $file->getClientOriginalExtension();
        $fileName = rand(11111, 99999) . '.' . $extension;
        $file_path = $file->move($destinationPath, $fileName);


        DB::table('hospital')
            ->where('id', $id)
            ->update(array(
                'name' => trim($input['name']),
                'address' => trim($input['address']),
                'city' => trim($input['city']),
                'district' => trim($input['district']),
                'image' => $file_path,
                'description' => trim($input['description']),
                'discount' => trim($input['discount']),
                'status' =>'1'
            ));
        return redirect('hospital');

    }
    public function show($id){

    }
    public function destroy($id){
        $hospital = Hospital::findOrFail($id);
//        dd($tools);
        $hospital->delete();

//        Session::flash('flash_message', 'Task successfully deleted!');

        return redirect()->route('hospital.index');
    }

    public function deleteSlider($id){

        print_r($id);

    }

}
