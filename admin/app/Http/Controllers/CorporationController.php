<?php

namespace App\Http\Controllers;


namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class CorporationController extends Controller
{
    public  function index(){
        $corporation= DB::table('corporation')->get();
        return view('corporation.index', compact('corporation', $corporation));
    }
    public function create(){
        return view('corporation.create');
    }
    public function store(){
        $input = Input::all();
        $file = array_get($input,'image');
        $destinationPath = 'uploads/corporation';
        $extension = $file->getClientOriginalExtension();
        $fileName = rand(11111, 99999) . '.' . $extension;
        $file_path = $file->move($destinationPath, $fileName);

        DB::table('corporation')->insert(array(
            'title' => trim($input['title']),
            'image' => $file_path,
            'text' => trim($input['text']),
            'status' =>'1'
        ));
        return redirect('corporation');

    }
    public function edit($id){
        $corporation = DB::table('corporation')->where('id', $id)->first();
//        dd($tools);
        return view('corporation.edit', compact('corporation', $corporation));

    }
    public function update($id){
        $input = Input::all();
        $file = array_get($input,'image');
        $destinationPath = 'uploads/corporation';
        $extension = $file->getClientOriginalExtension();
        $fileName = rand(11111, 99999) . '.' . $extension;
        $file_path = $file->move($destinationPath, $fileName);


        DB::table('corporation')
            ->where('id', $id)
            ->update(array(
                'title' => trim($input['title']),
                'image' => $file_path,
                'text' => trim($input['text']),
                'status' =>'1'
            ));
        return redirect('corporation');
    }
    public function show($id){

    }
    public function destroy($id){
        $slider = DB::table('slider')->where('id',$id)->delete();
        if($slider){
            return redirect('slider');
        } else{
            echo "Failed to Delete";
        }
    }

    public function deleteSlider($id){

        print_r($id);

    }

}
