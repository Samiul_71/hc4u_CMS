<?php

namespace App\Http\Controllers;


namespace App\Http\Controllers;

use App\Upcoming_event;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class Upcoming_eventController extends Controller
{
    public  function index(){
        $upcoming_event= DB::table('upcoming_event')->get();
        return view('upcoming_event.index', compact('upcoming_event', $upcoming_event));
    }
    public function create(){
        return view('upcoming_event.create');
    }
    public function store(){
        $input = Input::all();
//        dd($input['title']);

        $file = array_get($input,'image');
        $destinationPath = 'uploads/upcoming_event';
        $extension = $file->getClientOriginalExtension();
        $fileName = rand(11111, 99999) . '.' . $extension;
        $file_path = $file->move($destinationPath, $fileName);
        

        DB::table('upcoming_event')->insert(array(
            'title' => trim($input['title']),
            'image' => $file_path,
            'description' => trim($input['description']),
            'status' =>'1'
        ));
        return redirect('upcoming_event');

    }
    public function edit($id){
        $upcoming_event = DB::table('upcoming_event')->where('id', $id)->first();
//        dd($tools);
        return view('upcoming_event.edit', compact('upcoming_event', $upcoming_event));
    }
    public function update($id){
        $input = Input::all();
        $file = array_get($input,'image');
        $destinationPath = 'uploads/upcoming_event';
        $extension = $file->getClientOriginalExtension();
        $fileName = rand(11111, 99999) . '.' . $extension;
        $file_path = $file->move($destinationPath, $fileName);


        DB::table('upcoming_event')
            ->where('id', $id)
            ->update(array(
                'title' => trim($input['title']),
                'image' => $file_path,
                'description' => trim($input['description']),
                'status' =>'1'
            ));
        return redirect('upcoming_event');
    }
    public function show($id){

    }
    public function destroy($id){
        $upcoming_event = Upcoming_event::findOrFail($id);
//        dd($tools);
        $upcoming_event->delete();

//        Session::flash('flash_message', 'Task successfully deleted!');

        return redirect()->route('upcoming_event.index');
    }

    public function deleteSlider($id){

        print_r($id);

    }

}
