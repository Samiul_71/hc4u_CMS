<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class LogoController extends Controller
{
    public function index(){
        $logo= DB::table('logo')->get();
        return view('logo.index', compact('logo', $logo));
    }
    public function create(){
        return view('logo.create');
    }
    public function store(){
        $input = Input::all();
        $file = array_get($input,'image');
        $destinationPath = 'uploads/logo';
        $extension = $file->getClientOriginalExtension();
        $fileName = rand(11111, 99999) . '.' . $extension;
        $file_path = $file->move($destinationPath, $fileName);

        DB::table('logo')->insert(array(
            'name' => trim($input['name']),
            'image' => $file_path,
            'description' => trim($input['description']),
            'status' =>'1'
        ));
        return redirect('logo');

    }
    public function edit($id){
//        dd($id);
        $logo = DB::table('logo')->where('id', $id)->first();
//        dd($tools);
        return view('logo.edit', compact('logo', $logo));
    }
    public function update($id){
        $input = Input::all();
        $file = array_get($input,'image');
        $destinationPath = 'uploads/logo';
        $extension = $file->getClientOriginalExtension();
        $fileName = rand(11111, 99999) . '.' . $extension;
        $file_path = $file->move($destinationPath, $fileName);


        DB::table('logo')
            ->where('id', $id)
            ->update(array(
                'name' => trim($input['name']),
                 'image' => $file_path,
                'description' => trim($input['description']),
                'status' =>'1'
            ));
        return redirect('logo');
    }
    public function show($id){

    }
    public function destroy($id){
        $logo = DB::table('logo')->where('id',$id)->delete();
        if($logo){
            return redirect('logo');
        } else{
            echo "Failed to Delete";
        }
    }
}
