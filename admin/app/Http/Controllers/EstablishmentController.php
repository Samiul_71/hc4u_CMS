<?php

namespace App\Http\Controllers;


namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class EstablishmentController extends Controller
{
    public  function index(){
        $establishment= DB::table('establishment')->get();
        return view('establishment.index', compact('establishment', $establishment));
    }
    public function create(){
        return view('establishment.create');
    }
    public function store(){
        $input = Input::all();
        $file = array_get($input,'image');
        $destinationPath = 'uploads/establishment';
        $extension = $file->getClientOriginalExtension();
        $fileName = rand(11111, 99999) . '.' . $extension;
        $file_path = $file->move($destinationPath, $fileName);

        DB::table('establishment')->insert(array(
            'title' => trim($input['title']),
            'sub_title' => trim($input['sub_title']),
            'description' => trim($input['description']),
            'image' => $file_path,
            'status' =>'1'
        ));
        return redirect('establishment');

    }
    public function edit($id){
        $establishment = DB::table('establishment')->where('id', $id)->first();
//        dd($tools);
        return view('establishment.edit', compact('establishment', $establishment));
    }
    public function update($id){
        $input = Input::all();
        $file = array_get($input,'image');
        $destinationPath = 'uploads/establishment';
        $extension = $file->getClientOriginalExtension();
        $fileName = rand(11111, 99999) . '.' . $extension;
        $file_path = $file->move($destinationPath, $fileName);


        DB::table('establishment')
            ->where('id', $id)
            ->update(array(
                'title' => trim($input['title']),
                'sub_title' => trim($input['sub_title']),
                'description' => trim($input['description']),
                'image' => $file_path,
                'status' =>'1'
            ));
        return redirect('establishment');
    }
    public function show($id){

    }
    public function destroy($id){
        $slider = DB::table('slider')->where('id',$id)->delete();
        if($slider){
            return redirect('slider');
        } else{
            echo "Failed to Delete";
        }
    }

    public function deleteSlider($id){

        print_r($id);

    }

}
