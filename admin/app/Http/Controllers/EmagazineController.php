<?php

namespace App\Http\Controllers;


namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class EmagazineController extends Controller
{
    public  function index(){
        $emagazine= DB::table('emagazine')->get();
        return view('emagazine.index', compact('emagazine', $emagazine));
    }
    public function create(){
        return view('emagazine.create');
    }
    public function store(){
        $input = Input::all();
        $file = array_get($input,'image');
        $destinationPath = 'uploads/emagazine';
        $extension = $file->getClientOriginalExtension();
        $fileName = rand(11111, 99999) . '.' . $extension;
        $file_path = $file->move($destinationPath, $fileName);

        DB::table('emagazine')->insert(array(
            'title' => trim($input['title']),
            'image' => $file_path,
            'description' => trim($input['description']),
            'status' =>'1'
        ));
        return redirect('emagazine');

    }
    public function edit($id){
        $emagazine = DB::table('emagazine')->where('id', $id)->first();
//        dd($tools);
        return view('emagazine.edit', compact('emagazine', $emagazine));
    }
    public function update($id){
        $input = Input::all();
        $file = array_get($input,'image');
        $destinationPath = 'uploads/emagazine';
        $extension = $file->getClientOriginalExtension();
        $fileName = rand(11111, 99999) . '.' . $extension;
        $file_path = $file->move($destinationPath, $fileName);


        DB::table('emagazine')
            ->where('id', $id)
            ->update(array(
                'title' => trim($input['title']),
                'image' => $file_path,
                'description' => trim($input['description']),
                'status' =>'1'
            ));
        return redirect('emagazine');
    }
    public function show($id){

    }
    public function destroy($id){
        $slider = DB::table('emagazine')->where('id',$id)->delete();
        if($slider){
            return redirect('emagazine');
        } else{
            echo "Failed to Delete";
        }
    }

    public function deleteSlider($id){

        print_r($id);

    }

}
