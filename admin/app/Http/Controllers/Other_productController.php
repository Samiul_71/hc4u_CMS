<?php

namespace App\Http\Controllers;


namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class Other_productController extends Controller
{
    public  function index(){
        $other_product= DB::table('others_products')->get();
        return view('other_product.index', compact('other_product', $other_product));
    }
    public function create(){
        return view('other_product.create');
    }
    public function store(){
        $input = Input::all();
        $file = array_get($input,'image');
        $destinationPath = 'uploads/other_product';
        $extension = $file->getClientOriginalExtension();
        $fileName = rand(11111, 99999) . '.' . $extension;
        $file_path = $file->move($destinationPath, $fileName);

        DB::table('others_products')->insert(array(
            'title' => trim($input['title']),
            'image' => $file_path,
            'description' => trim($input['description']),
            'status' =>'1'
        ));
        return redirect('other_product');

    }
    public function edit($id){
        $other_product = DB::table('others_products')->where('id', $id)->first();
//        dd($tools);
        return view('other_product.edit', compact('other_product', $other_product));
    }
    public function update($id){
        $input = Input::all();
        $file = array_get($input,'image');
        $destinationPath = 'uploads/other_product';
        $extension = $file->getClientOriginalExtension();
        $fileName = rand(11111, 99999) . '.' . $extension;
        $file_path = $file->move($destinationPath, $fileName);


        DB::table('others_products')
            ->where('id', $id)
            ->update(array(
                'title' => trim($input['title']),
                'image' => $file_path,
                'description' => trim($input['description']),
                'status' =>'1'
            ));
        return redirect('other_product');
    }
    public function show($id){

    }
    public function destroy($id){
        $slider = DB::table('slider')->where('id',$id)->delete();
        if($slider){
            return redirect('slider');
        } else{
            echo "Failed to Delete";
        }
    }

    public function deleteSlider($id){

        print_r($id);

    }

}
