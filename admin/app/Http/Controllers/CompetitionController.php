<?php

namespace App\Http\Controllers;


namespace App\Http\Controllers;

use App\Competition;
use App\Fair;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class CompetitionController extends Controller
{
    public  function index(){
        $competition= DB::table('competition')->get();
        return view('competition.index', compact('competition', $competition));
    }
    public function create(){
        return view('competition.create');
    }
    public function store(){
        $input = Input::all();
        $file = array_get($input,'image');
        $destinationPath = 'uploads/competition';
        $extension = $file->getClientOriginalExtension();
        $fileName = rand(11111, 99999) . '.' . $extension;
        $file_path = $file->move($destinationPath, $fileName);

        $file1 = array_get($input,'image1');
        $destinationPath1 = 'uploads/competition';
        $extension1 = $file1->getClientOriginalExtension();
        $fileName1 = rand(11111, 999999) . '.' . $extension1;
        $file_path1 = $file1->move($destinationPath1, $fileName1);

        $file2 = array_get($input,'image2');
        $destinationPath2 = 'uploads/competition';
        $extension2 = $file2->getClientOriginalExtension();
        $fileName2 = rand(11111, 999999) . '.' . $extension2;
        $file_path2 = $file2->move($destinationPath2, $fileName2);

        DB::table('competition')->insert(array(
            'title' => trim($input['title']),
            'image' => $file_path,
            'image1' => $file_path1,
            'image2' => $file_path2,
            'description' => trim($input['description']),
            'status' =>'1'
        ));
        return redirect('competition');

    }
    public function edit($id){
        $competition = DB::table('competition')->where('id', $id)->first();
//        dd($tools);
        return view('competition.edit', compact('competition', $competition));
    }
    public function update($id){
        $input = Input::all();
        $file = array_get($input,'image');
        $destinationPath = 'uploads/competition';
        $extension = $file->getClientOriginalExtension();
        $fileName = rand(11111, 99999) . '.' . $extension;
        $file_path = $file->move($destinationPath, $fileName);


        DB::table('competition')
            ->where('id', $id)
            ->update(array(
                'title' => trim($input['title']),
                'image' => $file_path,
                'description' => trim($input['description']),
                'status' =>'1'
            ));
        return redirect('competition');
    }
    public function show($id){

    }
    public function destroy($id){
        $competition = Competition::findOrFail($id);
//        dd($tools);
        $competition->delete();

//        Session::flash('flash_message', 'Task successfully deleted!');

        return redirect()->route('competition.index');

    }

    public function deleteSlider($id){

        print_r($id);

    }

}
