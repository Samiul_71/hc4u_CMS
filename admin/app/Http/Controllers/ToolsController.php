<?php

namespace App\Http\Controllers;

use App\Gallary;
use App\Tools;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class ToolsController extends Controller
{
    public function index(){
        $tools = DB::table('tools')->orderBy('id', 'ASC')->get();
        return view('tools.index', compact('tools', $tools));
    }
    public function create(){
        return view('tools.create');
    }
    public function store(){
        $input = Input::all();
//        dd($input);
        DB::table('tools')->insert(array(
            'flag' => trim($input['flag']),
            'title' => trim($input['title']),
            'content' => trim($input['content'])
        ));
        return redirect('tools');
    }
    public function edit($id){
//        dd($id);
        $tools = DB::table('tools')->where('id', $id)->first();
//        dd($tools);
        return view('tools.edit', compact('tools', $tools));
    }
    public function update($id){

        $input = Input::all();
        DB::table('tools')
            ->where('id', $id)
            ->update(array(
                'flag' => trim($input['flag']),
                'title' => trim($input['title']),
                'content' => trim($input['content'])
            ));
        return redirect('tools');
    }
    public function show($id){


        
    }
    public function destroy($id){
        $tools = Tools::findOrFail($id);
//        dd($tools);
        $tools->delete();

//        Session::flash('flash_message', 'Task successfully deleted!');

        return redirect()->route('tools.index');
//        $tools = DB::table('tools')->where('id',$id)->delete();
//        if($tools){
//            return redirect('tools');
//        } else{
//            echo "Failed to Delete";
//        }
    }
}
