<?php

namespace App\Http\Controllers;


namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class ElibraryController extends Controller
{
    public  function index(){
        $elibrary= DB::table('elibrary')->get();
        return view('elibrary.index', compact('elibrary', $elibrary));
    }
    public function create(){
        return view('elibrary.create');
    }
    public function store(){
        $input = Input::all();
        $file = array_get($input,'image');
        $destinationPath = 'uploads/elibrary';
        $extension = $file->getClientOriginalExtension();
        $fileName = rand(11111, 99999) . '.' . $extension;
        $file_path = $file->move($destinationPath, $fileName);

        DB::table('elibrary')->insert(array(
            'title' => trim($input['title']),
            'description' => trim($input['description']),
            'image' => $file_path,
            'status' =>'1'
        ));
        return redirect('elibrary');

    }
    public function edit($id){
        $elibrary= DB::table('elibrary')->where('id', $id)->first();
//        dd($tools);
        return view('elibrary.edit', compact('elibrary', $elibrary));
    }
    public function update($id){
        $input = Input::all();
        $file = array_get($input,'image');
        $destinationPath = 'uploads/elibrary';
        $extension = $file->getClientOriginalExtension();
        $fileName = rand(11111, 99999) . '.' . $extension;
        $file_path = $file->move($destinationPath, $fileName);


        DB::table('elibrary')
            ->where('id', $id)
            ->update(array(
                'title' => trim($input['title']),
                'description' => trim($input['description']),
                'image' => $file_path,
                'status' =>'1'
            ));
        return redirect('elibrary');
    }
    public function show($id){

    }
    public function destroy($id){
        $slider = DB::table('slider')->where('id',$id)->delete();
        if($slider){
            return redirect('slider');
        } else{
            echo "Failed to Delete";
        }
    }

    public function deleteSlider($id){

        print_r($id);

    }

}
