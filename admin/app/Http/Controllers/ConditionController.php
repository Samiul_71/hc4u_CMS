<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class ConditionController extends Controller
{
    public function index(){
        $condition = DB::table('condition')->orderBy('id', 'ASC')->get();
        return view('condition.index', compact('condition', $condition));
    }
    public function create(){
        return view('condition.create');
    }
    public function store(){
        $input = Input::all();
//        dd($input);
        DB::table('condition')->insert(array(
            'title' => trim($input['title']),
            'description' => trim($input['description'])
        ));
        return redirect('condition');
    }
    public function edit($id){
//        dd($id);
        $condition = DB::table('condition')->where('id', $id)->first();
//        dd($tools);
        return view('condition.edit', compact('condition', $condition));
    }
    public function update($id){

        $input = Input::all();
        DB::table('condition')
            ->where('id', $id)
            ->update(array(
                'title' => trim($input['title']),
                'description' => trim($input['description'])
            ));
        return redirect('condition');
    }
    public function show($id){
      dd($id);
        $tools = DB::table('tools')->where('id', $id)->first();
//        dd($tools);
        
    }
    public function destroy($id){

    }
}
