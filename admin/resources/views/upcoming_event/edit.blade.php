@extends('admin_layout/home')
<!-- content -->
@section('content')
    <div class="col-md-10">
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h4>Upcoming Event Update</h4>
                    </div>
                    <div class="panel-body">
                        <div class="bootstrap-admin-no-table-panel-content bootstrap-admin-panel-content collapse in">
                            {{--{!! Form::model($logo, ['method' => 'PATCH', 'action' => ['LogoController@update', 'logo/' => $logo->id  ]]) !!}--}}
                            {!! Form::model($upcoming_event, ['route' => ['upcoming_event.update', $upcoming_event->id], 'method' => 'PATCH', 'files'=>true]) !!}
                            <fieldset>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label" for="flag">Name </label>
                                    <div class="col-lg-10">
                                        <input type="text" name="title" class="form-control col-md-6" id="title" value="{!! $upcoming_event->title !!}">
                                    </div>
                                </div>
                                <br><br>
                                {{--<div class="form-group">--}}
                                {{--<label class="col-lg-2 control-label" for="fileInput">Logo</label>--}}
                                {{--<td class="table-text"><img src="{{ $logo->image }}" height="40" width="40"></td>--}}
                                {{--<div class="col-lg-10">--}}
                                {{--<img src="{{ $logo->image }}" height="40" width="40">--}}
                                {{--<img src="uploads/logo/32236.jpg">--}}
                                {{--<img src="../../../public/uploads/logo/32236.jpg" height="40" width="40">--}}
                                {{--<input class="form-control uniform_on" id="fileInput" type="file" name="image">--}}
                                {{--</div>--}}
                                {{--</div>--}}
                                <div class="form-group">
                                    <label class="col-lg-2 control-label" for="fileInput">Image</label>
                                    <div class="col-lg-10">
                                        <input class="form-control uniform_on" id="fileInput" type="file" name="image">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label" for="tinymce_full">Description </label>
                                    <div class="col-lg-10">
                                        <div class="bootstrap-admin-panel-content">
                                            <textarea id="tinymce_full" name="description">
                                               {!! $upcoming_event->description !!}
                                            </textarea>
                                        </div>
                                    </div>
                                </div>


                                {{--<div class="form-group">--}}
                                {{--<label class="col-lg-2 control-label" for="fileInput">Image</label>--}}
                                {{--<div class="col-lg-10">--}}
                                {{--<input class="form-control uniform_on" id="fileInput" type="file">--}}
                                {{--</div>--}}
                                {{--</div>--}}

                                <div class="pull-right">
                                    <button type="submit" class="btn btn-primary">Save Data</button>
                                    <button type="reset" class="btn btn-default">Cancel</button>
                                </div>

                            </fieldset>
                            {{--{{ Form::close() }}--}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
<!-- content -->