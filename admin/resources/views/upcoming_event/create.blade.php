@extends('admin_layout/home')
<!-- content -->
@section('content')
    <div class="col-md-10">
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h4>Upcoming Event Data Add</h4>
                    </div>
                    <div class="panel-body">
                        <div class="bootstrap-admin-no-table-panel-content bootstrap-admin-panel-content collapse in">
                            {!! Form::open(array('route' => 'upcoming_event.store', 'class' => 'form-horizontal', 'files' => true)) !!}
                                <div class="form-group">
                                    <label class="col-lg-2 control-label" for="title">Name</label>
                                    <div class="col-lg-10">
                                        <input class="form-control uniform_on" id="title" type="text" name="title">
                                    </div>
                                </div><br>
                            <div class="form-group">
                                <label class="col-lg-2 control-label" for="fileInput">Image</label>
                                <div class="col-lg-10">
                                    <input class="form-control uniform_on" id="fileInput" type="file" name="image">
                                    <span style="-moz-user-select: none;" class="action">Choose File</span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-2 control-label" for="tinymce_full">Description </label>
                                <div class="col-lg-10">
                                    <div class="bootstrap-admin-panel-content">
                                        <textarea id="tinymce_full" name="description"></textarea>
                                    </div>
                                </div>
                            </div>

                                <div class="pull-right">
                                    <button type="submit" class="btn btn-primary">Save Data</button>
                                    <button type="reset" class="btn btn-default">Cancel</button>
                                </div>
                            {{--{{ Form::close() }}--}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
<!-- content -->