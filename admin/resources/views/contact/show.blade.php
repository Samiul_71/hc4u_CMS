@extends('admin_layout/home')
<!-- content -->
@section('content')
    <div class="col-md-10">
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h4>Tools Update</h4>
                    </div>
                    <div class="panel-body">
                        <div class="bootstrap-admin-no-table-panel-content bootstrap-admin-panel-content collapse in">
                            {!! Form::model($tools, ['method' => 'PATCH', 'action' => ['ToolsController@update', 'tools/' => $tools->id  ]]) !!}
                            <fieldset>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label" for="flag">Sample Title </label>
                                    <div class="col-lg-10">
                                        <input type="text" name="flag" class="form-control col-md-6" id="flag" value="{!! $tools->flag !!}">
                                    </div>
                                </div>
                                <br><br>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label" for="title">Title </label>
                                    <div class="col-lg-10">
                                        <input type="text" name="title" class="form-control col-md-6" id="title" value="{!! $tools->title !!}">
                                    </div>
                                </div>
                                {{--<div class="form-group">--}}
                                {{--<label class="col-lg-2 control-label" for="fileInput">Image</label>--}}
                                {{--<div class="col-lg-10">--}}
                                {{--<input class="form-control uniform_on" id="fileInput" type="file">--}}
                                {{--</div>--}}
                                {{--</div>--}}
                                <div class="form-group">
                                    <label class="col-lg-2 control-label" for="tinymce_full">Description </label>
                                    <div class="col-lg-10">
                                        <div class="bootstrap-admin-panel-content">
                                            <textarea id="tinymce_full" name="content">
                                               {!! $tools->content !!}
                                            </textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="pull-right">
                                    <button type="submit" class="btn btn-primary">Save Data</button>
                                    <button type="reset" class="btn btn-default">Cancel</button>
                                </div>

                            </fieldset>
                            {{ Form::close() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
<!-- content -->