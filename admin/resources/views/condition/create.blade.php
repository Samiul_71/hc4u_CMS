@extends('admin_layout/home')
<!-- content -->
@section('content')
    <div class="col-md-10">
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h4>Terms Ans Conditions Info Add</h4>
                    </div>
                    <div class="panel-body">
                        <div class="bootstrap-admin-no-table-panel-content bootstrap-admin-panel-content collapse in">
                                {{--{{ Form::open(array('route' => 'tools.store', 'class' => 'form-horizontal'))  }}--}}
                            {!! Form::open(['route' => 'condition.store'], array('class' => 'form-horizontal')) !!}
                                <fieldset>
                                    {{--<legend>Tools Add</legend>--}}

                                    <div class="form-group">
                                        <label class="col-lg-2 control-label" for="title">Title </label>
                                        <div class="col-lg-10">
                                            <input type="text" name="title" class="form-control col-md-6" id="title" autocomplete="off" data-provide="typeahead" data-items="4" data-source='["Alabama","Alaska","Arizona","Arkansas","California","Colorado","Connecticut","Delaware","Florida","Georgia","Hawaii","Idaho","Illinois","Indiana","Iowa","Kansas","Kentucky","Louisiana","Maine","Maryland","Massachusetts","Michigan","Minnesota","Mississippi","Missouri","Montana","Nebraska","Nevada","New Hampshire","New Jersey","New Mexico","New York","North Dakota","North Carolina","Ohio","Oklahoma","Oregon","Pennsylvania","Rhode Island","South Carolina","South Dakota","Tennessee","Texas","Utah","Vermont","Virginia","Washington","West Virginia","Wisconsin","Wyoming"]'>

                                        </div>
                                    </div>
                                    {{--<div class="form-group">--}}
                                        {{--<label class="col-lg-2 control-label" for="fileInput">Image</label>--}}
                                        {{--<div class="col-lg-10">--}}
                                            {{--<input class="form-control uniform_on" id="fileInput" type="file">--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                    <div class="form-group">
                                        <label class="col-lg-2 control-label" for="tinymce_full">Description </label>
                                        <div class="col-lg-10">
                                            <div class="bootstrap-admin-panel-content">
                                                <textarea id="tinymce_full" name="description"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="pull-right">
                                        <button type="submit" class="btn btn-primary">Save Data</button>
                                        <button type="reset" class="btn btn-default">Cancel</button>
                                    </div>

                                </fieldset>
                            {{--{{ Form::close() }}--}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
<!-- content -->


