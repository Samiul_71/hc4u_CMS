@extends('admin_layout/home')
<!-- content -->
@section('content')
    <div class="col-md-10">
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h4>Logo Add</h4>
                    </div>
                    <div class="panel-body">
                        <div class="bootstrap-admin-no-table-panel-content bootstrap-admin-panel-content collapse in">
                            {!! Form::open(array('route' => 'logo.store', 'class' => 'form-horizontal', 'files' => true)) !!}
                                <div class="form-group">
                                    <label class="col-lg-2 control-label" for="name">Name</label>
                                    <div class="col-lg-10">
                                        <input class="form-control uniform_on" id="name" type="text" name="name">
                                    </div>
                                </div><br><br>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label" for="fileInput">Logo</label>
                                    <div class="col-lg-10">
                                        <input class="form-control uniform_on" id="fileInput" type="file" name="image">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label" for="description">Description </label>
                                    <div class="col-lg-10">
                                        <div class="bootstrap-admin-panel-content">
                                            <textarea id="description" name="description"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="pull-right">
                                    <button type="submit" class="btn btn-primary">Save Data</button>
                                    <button type="reset" class="btn btn-default">Cancel</button>
                                </div>
                            {{--{{ Form::close() }}--}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
<!-- content -->