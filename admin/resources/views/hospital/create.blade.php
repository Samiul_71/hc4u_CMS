@extends('admin_layout/home')
<!-- content -->
@section('content')
    <div class="col-md-10">
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h4>Hospital Add</h4>
                    </div>
                    <div class="panel-body">
                        <div class="bootstrap-admin-no-table-panel-content bootstrap-admin-panel-content collapse in">
                            {!! Form::open(array('route' => 'hospital.store', 'class' => 'form-horizontal', 'files' => true)) !!}
                                <div class="form-group">
                                    <label class="col-lg-2 control-label" for="title">Name</label>
                                    <div class="col-lg-10">
                                        <input class="form-control uniform_on" id="title" type="text" name="name">
                                    </div>
                                </div><br>
                            <div class="form-group">
                                <label class="col-lg-2 control-label" for="tinymce_full1">Address </label>
                                <div class="col-lg-10">
                                    <div class="bootstrap-admin-panel-content">
                                        <textarea id="tinymce_full1" name="address"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label" for="city">City</label>
                                <div class="col-lg-10">
                                    <select id="city" class="chzn-select" style="width: 150px" name="city">
                                        <option>Select City</option>
                                        <option name="">Dhaka</option>
                                        <option>Chittagong</option>
                                        <option>Mymensing</option>
                                        <option>Rangpur</option>
                                        <option>Rajshahi</option>
                                        <option>Barishal</option>
                                        <option>Khulna</option>
                                    </select>
                                </div>
                            </div>

                            {{--<div class="form-group">--}}
                                {{--<label class="col-lg-2 control-label" for="title">City</label>--}}
                                {{--<div class="col-lg-10">--}}
                                    {{--<input class="form-control uniform_on" id="title" type="text" name="city">--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            <br>

                            <div class="form-group">
                                <label class="col-lg-2 control-label" for="title">District</label>
                                <div class="col-lg-10">
                                    <input class="form-control uniform_on" id="title" type="text" name="district">
                                </div>
                            </div><br>


                            <div class="form-group">
                                <label class="col-lg-2 control-label" for="fileInput">Image</label>
                                <div class="col-lg-10">
                                    <input class="form-control uniform_on" id="fileInput" type="file" name="image">
                                </div>
                            </div><br>
                            <div class="form-group">
                                <label class="col-lg-2 control-label" for="tinymce_full2">Description </label>
                                <div class="col-lg-10">
                                    <div class="bootstrap-admin-panel-content">
                                        <textarea id="tinymce_full2" name="description"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label" for="tinymce_full3">Discount </label>
                                <div class="col-lg-10">
                                    <div class="bootstrap-admin-panel-content">
                                        <textarea id="tinymce_full3" name="discount"></textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="pull-right">
                                    <button type="submit" class="btn btn-primary">Save Data</button>
                                    <button type="reset" class="btn btn-default">Cancel</button>
                                </div>
                            {{--{{ Form::close() }}--}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
<!-- content -->
