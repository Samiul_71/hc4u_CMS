@extends('admin_layout/home')
@section('content')
    <!-- content -->
    <div class="col-md-10">
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="text-muted bootstrap-admin-box-title">Hospital List</div>
                    </div>
                    <div class="bootstrap-admin-panel-content">

                        <a href="{{ URL::to('hospital/create') }}">
                            <button class="pull-right btn btn-default">Create</button>
                        </a>
                        <br><br>
                        <table class="table table-striped table-bordered" id="example">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Address</th>
                                <th>City</th>
                                <th>District</th>
                                <th>Image</th>
                                <th>Description</th>
                                <th>Discount</th>
                                <th>Actions</th>
                                {{--<th>Engine</th>--}}
                                {{--<th>Actions</th>--}}
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($hospital as $data)
                                <tr>
                                    <!-- Task Name -->
                                    <td class="table-text">{{ $data->name }}</td>
                                    <td class="table-text">{{ strip_tags($data->address) }}</td>
                                    <td class="table-text">{{ $data->city }}</td>
                                    <td class="table-text">{{ $data->district }}</td>
                                    <td class="table-text"><img src="{{ $data->image }}" height="40" width="40"></td>
                                    <td class="table-text">{{ strip_tags($data->description) }}</td>
                                    <td class="table-text">{{ strip_tags($data->discount) }}</td>
                                    <td class="actions">
                                        <a href="{{ URL::to('hospital/'.$data->id).'/edit'}}"><button class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-pencil"></i></button></a>
                                        {{--<a  href="{{ URL::to('hospital/'.$data->id)}}"><button class="btn btn-sm btn-danger"><i class="glyphicon glyphicon-user"></i></button></a>--}}

                                        {{--<div class="delete" data-id="{{ $data->id}}"><button class="btn btn-sm btn-info"><i class="glyphicon glyphicon-trash"></i></button> </div>--}}
                                        {{--{{ Form::open(array('url' => 'slider' . $data->id,'class' => 'pull-right' )) }}--}}
                                        {{--{{ Form::hidden('_method', 'distroy') }}--}}
                                        {{--{{ Form::submit('Delete this Nerd', array('class' => 'glyphicon glyphicon-trash')) }}--}}
                                        {{--{{ Form::close() }}--}}
                                    </td>
                                    <td class="actions">
                                        {!! Form::open([
                                                    'method' => 'DELETE',
                                                    'route' => ['hospital.destroy', $data->id]
                                                ]) !!}
                                        {!! Form::submit('Delete', ['class' => 'btn btn-sm btn-danger ']) !!}

                                        {!! Form::close() !!}
                                        {{--<a href="{{ URL::to('gallary/'.$data->id)}}"><button class="btn btn-sm btn-info"><i class="glyphicon glyphicon-trash"></i></button></a>--}}
                                    </td>

                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- content -->
@endsection
<script>
    $('#example tbody').on('click', 'delete', function () {
        var r = confirm('Are you sure, you want to delete?');
        if(r){
            var slider_id = $(this).attr('data-id');
            $.post('slider/delete',{id:slider_id},function(response){

            })}
    });

</script>