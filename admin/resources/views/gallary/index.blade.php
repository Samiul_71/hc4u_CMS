@extends('admin_layout/home')
@section('content')
    <!-- content -->
    <div class="col-md-10">
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="text-muted bootstrap-admin-box-title">Gallary List</div>
                    </div>
                    <div class="bootstrap-admin-panel-content">

                        <a href="{{ URL::to('gallary/create') }}">
                            <button class="pull-right btn btn-default">Create</button>
                        </a>
                        <br><br>
                        <table class="table table-striped table-bordered" id="example">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Image</th>
                                <th>Description</th>
                                <th>Actions</th>
                                {{--<th>Engine</th>--}}
                                {{--<th>Actions</th>--}}
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($gallary as $data)
                                <tr>
                                    <!-- Task Name -->
                                    <td class="table-text">{{ $data->title }}</td>
                                    <td class="table-text"><img src="{{ $data->image }}" height="40" width="40"></td>
                                    <td class="table-text">{{ strip_tags($data->content) }}</td>
                                    <td class="actions">
                                        <a href="{{ URL::to('gallary/'.$data->id).'/edit'}}"><button class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-pencil"></i></button></a>
                                     </td>
                                    {{--<td class="actions">--}}
                                        {{--<a  href="{{ URL::to('gallary/'.$data->id)}}"><button class="btn btn-sm btn-danger"><i class="glyphicon glyphicon-user"></i></button></a>--}}
                                     {{--</td>--}}
                                        {{--<div class="delete" data-id="{{ $data->id}}"><button class="btn btn-sm btn-info"><i class="glyphicon glyphicon-trash"></i></button> </div>--}}
                                        {{--{{ Form::open(array('url' => 'slider' . $data->id,'class' => 'pull-right' )) }}--}}
                                        {{--{{ Form::hidden('_method', 'distroy') }}--}}
                                        {{--{{ Form::submit('Delete this Nerd', array('class' => 'glyphicon glyphicon-trash')) }}--}}
                                        {{--{{ Form::close() }}--}}
                                    <td class="actions">
                                        {!! Form::open([
                                                    'method' => 'DELETE',
                                                    'route' => ['gallary.destroy', $data->id]
                                                ]) !!}
                                        {!! Form::submit('Delete', ['class' => 'btn btn-sm btn-danger glyphicon glyphicon-user']) !!}

                                        {!! Form::close() !!}
                                        {{--<a href="{{ URL::to('gallary/'.$data->id)}}"><button class="btn btn-sm btn-info"><i class="glyphicon glyphicon-trash"></i></button></a>--}}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- content -->
@endsection
<script>
    $('#example tbody').on('click', 'delete', function () {
        var r = confirm('Are you sure, you want to delete?');
        if(r){
            var slider_id = $(this).attr('data-id');
            $.post('slider/delete',{id:slider_id},function(response){

            })}
    });

</script>