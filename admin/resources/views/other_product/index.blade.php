@extends('admin_layout/home')
@section('content')
    <!-- content -->
    <div class="col-md-10">
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="text-muted bootstrap-admin-box-title">Others Product List</div>
                    </div>
                    <div class="bootstrap-admin-panel-content">

                        <a href="{{ URL::to('other_product/create') }}">
                            <button class="pull-right btn btn-default">Create</button>
                        </a>
                        <br><br>
                        <table class="table table-striped table-bordered" id="example">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Image</th>
                                <th>Description</th>
                                <th colspan="3">Actions</th>
                                {{--<th>Engine</th>--}}
                                {{--<th>Actions</th>--}}
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($other_product as $data)
                                <tr>
                                    <!-- Task Name -->
                                    <td class="table-text">{{ $data->title }}</td>
                                    <td class="table-text"><img src="{{ $data->image }}" height="40" width="40"></td>
                                    <td class="table-text">{{ strip_tags($data->description) }}</td>
                                    <td class="action">
                                        <a href="{{ URL::to('other_product/'.$data->id).'/edit'}}"><button class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-pencil"></i></button></a>
                                    </td>
                                    <td class="action">
                                        <a  href="{{ URL::to('other_product/'.$data->id)}}"><button class="btn btn-sm btn-danger"><i class="glyphicon glyphicon-user"></i></button></a>
                                    </td>
                                    <td class="action">
                                        {{--<div class="delete" data-id="{{ $data->id}}" style="cursor:pointer; color:#676bdf;"><i class="glyphicon glyphicon-trash"></i></div>--}}
                                        <button id="delete" class="btn btn-sm btn-info" data-id="{{ $data->id}}"><i class="glyphicon glyphicon-trash"></i></button>
                                    </td>
                                    {{--<td class="actions">--}}
                                        {{----}}
                                        {{----}}

                                        {{--<div class="delete" data-id="{{ $data->id}}"><button class="btn btn-sm btn-info"><i class="glyphicon glyphicon-trash"></i></button> </div>--}}


                                        {{--<button id="delete" class="btn btn-sm btn-info" data-id="{{ $data->id}}"><i class="glyphicon glyphicon-trash"></i></button>--}}
                                    {{--</td>--}}
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- content -->
@endsection
<script>
    $(document).ready(function() {
        $('#delete').on('click', function () {
            alert('ok');
            var r = confirm('Are You Sure !!! You Want to Delete?');
            if (r) {
                var employee_id = $(this).attr('data-id');
                $.post('employee/delete', {id: employee_id}, function (response) {
                    if ($.trim(response) == 'Data Deleted') {
                        location.reload();
                    } else {

                    }
                });
            }
        });
    });
</script>