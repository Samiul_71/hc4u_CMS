@extends('admin_layout/home')
@section('content')
    <!-- content -->
    <div class="col-md-10">
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="text-muted bootstrap-admin-box-title">Tools List</div>
                    </div>
                    <div class="bootstrap-admin-panel-content">

                        <a href="{{ URL::to('tools/create') }}">
                            <button class="pull-right btn btn-default">Create</button>
                        </a>
                        <br><br>
                        <table class="table table-striped table-bordered" id="example">
                            <thead>
                            <tr>
                                <th>Flag</th>
                                <th>Title</th>
                                <th>Content</th>
                                {{--<th>Engine</th>--}}
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($tools as $data)
                                <tr>
                                    <!-- Task Name -->
                                    <td class="table-text">{{ $data->flag }}</td>
                                    <td class="table-text">{{ $data->title }}</td>
                                    <td class="table-text">{{ strip_tags($data->content) }}</td>
                                    {{--<td class="table-text">{{ $data->content }}</td>--}}

                                    <td class="actions">
                                        <a href="{{ URL::to('tools/'.$data->id).'/edit'}}"><button class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-pencil"></i></button></a>
                                        {{--<a href="{{ URL::to('tools/'.$data->id).'/show'}}"><button class="btn btn-sm btn-danger"><i class="glyphicon glyphicon-user"></i></button></a>--}}
                                    </td>
                                    <td class="actions">
                                        {!! Form::open([
                                                    'method' => 'DELETE',
                                                    'route' => ['tools.destroy', $data->id]
                                                ]) !!}
                                        {!! Form::submit('Delete', ['class' => 'btn btn-sm btn-danger ']) !!}

                                        {!! Form::close() !!}
                                        {{--<a href="{{ URL::to('gallary/'.$data->id)}}"><button class="btn btn-sm btn-info"><i class="glyphicon glyphicon-trash"></i></button></a>--}}
                                    </td>


                                </tr>
                            @endforeach
                            {{--<tr class="odd gradeX">--}}
                            {{--<td>Trident</td>--}}
                            {{--<td>Internet Explorer 4.0</td>--}}
                            {{--<td>Win 95+</td>--}}
                            {{--<td class="center"> 4</td>--}}
                            {{--<td class="actions">--}}
                            {{--<a href="#"><button class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-pencil"></i></button></a>--}}
                            {{--<a href="#"><button class="btn btn-sm btn-success"><i class="glyphicon glyphicon-ok-sign"></i></button></a>--}}
                            {{--<a href="#"><button class="btn btn-sm btn-warning"><i class="glyphicon glyphicon-bell"></i></button></a>--}}
                            {{--<a href="#"><button class="btn btn-sm btn-danger"><i class="glyphicon glyphicon-user"></i></button></a>--}}
                            {{--<a href="#"><button class="btn btn-sm btn-default"><i class="glyphicon glyphicon-gift"></i></button></a>--}}
                            {{--<a href="#"><button class="btn btn-sm btn-info"><i class="glyphicon glyphicon-trash"></i></button></a>--}}
                            {{--</td>--}}
                            {{--</tr>--}}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- content -->
@endsection