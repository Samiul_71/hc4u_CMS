@extends('admin_layout/home')
@section('content')
    <!-- content -->
    <div class="col-md-10">
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="text-muted bootstrap-admin-box-title">Fair</div>
                    </div>
                    <div class="bootstrap-admin-panel-content">

                        <a href="{{ URL::to('fair/create') }}">
                            <button class="pull-right btn btn-default">Create</button>
                        </a>
                        <br><br>
                        <table class="table table-striped table-bordered" id="example">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Image1</th>
                                <th>Image2</th>
                                <th>Image3</th>
                                <th>Description</th>
                                <th>Actions</th>
                                {{--<th>Engine</th>--}}
                                {{--<th>Actions</th>--}}
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($fair as $data)
                                <tr>
                                    <!-- Task Name -->
                                    <td class="table-text">{{ $data->title }}</td>
                                    <td class="table-text"><img src="{{ $data->image }}" height="40" width="40"></td>
                                    <td class="table-text"><img src="{{ $data->image1 }}" height="40" width="40"></td>
                                    <td class="table-text"><img src="{{ $data->image2 }}" height="40" width="40"></td>
                                    <td class="table-text">{{ strip_tags($data->description) }}</td>
                                    <td class="actions">
                                        <a href="{{ URL::to('fair/'.$data->id).'/edit'}}"><button class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-pencil"></i></button></a>
                                        {{--<a href="{{ URL::to('fair/'.$data->id).'/show'}}"><button class="btn btn-sm btn-danger"><i class="glyphicon glyphicon-user"></i></button></a>--}}
                                    </td>
                                    <td class="actions">
                                        {!! Form::open([
                                                    'method' => 'DELETE',
                                                    'route' => ['fair.destroy', $data->id]
                                                ]) !!}
                                        {!! Form::submit('Delete', ['class' => 'btn btn-sm btn-danger ']) !!}

                                        {!! Form::close() !!}
                                        {{--<a href="{{ URL::to('gallary/'.$data->id)}}"><button class="btn btn-sm btn-info"><i class="glyphicon glyphicon-trash"></i></button></a>--}}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- content -->
@endsection