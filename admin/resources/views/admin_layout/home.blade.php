<!DOCTYPE html>
<html>
<head>
    <title>Admin Panel | Hc4ubd</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Bootstrap Docs -->
    <link href="http://getbootstrap.com/docs-assets/css/docs.css" rel="stylesheet" media="screen">

    <!-- Bootstrap -->
    {!! HTML::style('assets/admin_template/css/bootstrap.min.css') !!}
    {!! HTML::style('assets/admin_template/css/bootstrap-theme.min.css') !!}


    <!-- Bootstrap Admin Theme -->
    {!! HTML::style('assets/admin_template/css/bootstrap-admin-theme.css') !!}
    {!! HTML::style('assets/admin_template/css/bootstrap-admin-theme-change-size.css') !!}
    {!! HTML::style('assets/admin_template/css/DT_bootstrap.css') !!}

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->

    <!--[if lt IE 9]>
    {!! HTML::script('assets/admin_template/js/html5shiv.js') !!}
    {!! HTML::script('assets/admin_template/js/respond.min.js') !!}

    {!! HTML::script('assets/admin_template/plugins/easypiechart/jquery.easy-pie-chart.css') !!}
    {!! HTML::script('assets/admin_template/plugins/easypiechart/jquery.easy-pie-chart_custom.css') !!}
    <![endif]-->

    <!-- Custom styles -->
    <style type="text/css">
        @font-face {
            font-family: Ubuntu;
            /*src: url('fonts/Ubuntu-Regular.ttf');*/
        }
        .bs-docs-masthead{
            background-color: #6f5499;
            background-image: linear-gradient(to bottom, #563d7c 0px, #6f5499 100%);
            background-repeat: repeat-x;
        }
        .bs-docs-masthead{
            padding: 0;
        }
        .bs-docs-masthead h1{
            color: #fff;
            font-size: 40px;
            margin: 0;
            padding: 34px 0;
            text-align: center;
        }
        .bs-docs-masthead a:hover{
            text-decoration: none;
        }
        .meritoo-logo a{
            background-color: #fff;
            border: 1px solid rgba(66, 139, 202, 0.4);
            display: block;
            font-family: Ubuntu;
            padding: 22px 0;
            text-align: center;
        }
        .meritoo-logo a,
        .meritoo-logo a:hover,
        .meritoo-logo a:focus{
            text-decoration: none;
        }
        .meritoo-logo a img{
            display: block;
            margin: 0 auto;
        }
        .meritoo-logo a span{
            color: #4e4b4b;
            font-size: 18px;
        }
        .row-urls{
            margin-top: 4px;
        }
        .row-urls .col-md-6{
            text-align: center;
        }
        .row-urls .col-md-6 a{
            font-size: 14px;
        }
    </style>

</head>
<body>

<!-- navbar -->
<nav class="navbar navbar-default navbar-fixed-top bootstrap-admin-navbar" role="navigation">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".main-navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="about.html">Admin Panel</a>
                </div>
                <div class="collapse navbar-collapse main-navbar-collapse">
                    <ul class="nav navbar-nav pull-right">
                        {{--<li><a href="#">Link</a></li>--}}
                        {{--<li><a href="#">Link</a></li>--}}
                        {{--<li class="dropdown">--}}
                            {{--<a href="#" class="dropdown-toggle" data-hover="dropdown">Dropdown <b class="caret"></b></a>--}}
                            {{--<ul class="dropdown-menu">--}}
                                {{--<li role="presentation" class="dropdown-header">Dropdown header</li>--}}
                                {{--<li><a href="#">Action</a></li>--}}
                                {{--<li><a href="#">Another action</a></li>--}}
                                {{--<li><a href="#">Something else here</a></li>--}}
                                {{--<li role="presentation" class="divider"></li>--}}
                                {{--<li role="presentation" class="dropdown-header">Dropdown header</li>--}}
                                {{--<li><a href="#">Separated link</a></li>--}}
                                {{--<li><a href="#">One more separated link</a></li>--}}
                            {{--</ul>--}}
                        {{--</li>--}}
                        <li class="dropdown">
                            <a href="#" role="button" class="dropdown-toggle" data-hover="dropdown"> <i class="glyphicon glyphicon-user"></i> Username <i class="caret"></i></a>
                            <ul class="dropdown-menu">
                                <li><a href="#">Action</a></li>
                                {{--<li><a href="#">Another action</a></li>--}}
                                {{--<li><a href="#">Something else here</a></li>--}}
                                <li role="presentation" class="divider"></li>
                                <li><a href="logout">Logout</a></li>
                            </ul>
                        </li>
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div>
        </div>
    </div><!-- /.container -->
</nav>

<div class="container">
    <!-- left, vertical navbar & content -->
    <div class="row">
        <!-- left, vertical navbar -->
        <div class="col-md-2 bootstrap-admin-col-left">
            <ul class="nav navbar-collapse collapse bootstrap-admin-navbar-side">
                <li><a href="{{ URL::to('/') }}"><i class="glyphicon glyphicon-chevron-right"></i>Dashboard</a></li>
                <li><a href="{{ URL::to('logo') }}"><i class="glyphicon glyphicon-chevron-right"></i>Logo</a></li>
                <li><a href="{{ URL::to('about') }}"><i class="glyphicon glyphicon-chevron-right"></i>About Us</a></li>
                <li><a href="{{ URL::to('slider') }}"><i class="glyphicon glyphicon-chevron-right"></i>Slider</a></li>
                <li><a href="{{ URL::to('tools') }}"><i class="glyphicon glyphicon-chevron-right"></i>Tools</a></li>
                <li><a href="{{ URL::to('gallary') }}"><i class="glyphicon glyphicon-chevron-right"></i>Gallary</a></li>
                <li><a href="{{ URL::to('hospital') }}"><i class="glyphicon glyphicon-chevron-right"></i>Hospital</a></li>
                <li><a href="{{ URL::to('corporation') }}"><i class="glyphicon glyphicon-chevron-right"></i>Corporation</a></li>
                <li><a href="{{ URL::to('main_product') }}"><i class="glyphicon glyphicon-chevron-right"></i>Main Products</a></li>
                <li><a href="{{ URL::to('other_product') }}"><i class="glyphicon glyphicon-chevron-right"></i>Other Products</a></li>
                <li><a href="{{ URL::to('csr') }}"><i class="glyphicon glyphicon-chevron-right"></i>CSR</a></li>
                <li><a href="{{ URL::to('fair') }}"><i class="glyphicon glyphicon-chevron-right"></i>Fair</a></li>
                <li><a href="{{ URL::to('competition') }}"><i class="glyphicon glyphicon-chevron-right"></i>Competition</a></li>
                <li><a href="{{ URL::to('upcoming_event') }}"><i class="glyphicon glyphicon-chevron-right"></i>Upcoming Event</a></li>
                <li><a href="{{ URL::to('emagazine') }}"><i class="glyphicon glyphicon-chevron-right"></i>eMagazine</a></li>
                <li><a href="{{ URL::to('elibrary') }}"><i class="glyphicon glyphicon-chevron-right"></i>eLibrary</a></li>
                <li><a href="{{ URL::to('establishment') }}"><i class="glyphicon glyphicon-chevron-right"></i>Establishment</a></li>
                <li><a href="{{ URL::to('achivement') }}"><i class="glyphicon glyphicon-chevron-right"></i>Achivement</a></li>
                <li><a href="{{ URL::to('publication') }}"><i class="glyphicon glyphicon-chevron-right"></i>Publication</a></li>
                <li><a href="{{ URL::to('contact') }}"><i class="glyphicon glyphicon-chevron-right"></i>Contact Us</a></li>
                <li><a href="{{ URL::to('condition') }}"><i class="glyphicon glyphicon-chevron-right"></i>Conditions</a></li>

                {{--<li>--}}
                    {{--<a href="forms.html"><i class="glyphicon glyphicon-chevron-down"></i> Submenu</a>--}}
                    {{--<ul class="nav navbar-collapse bootstrap-admin-navbar-side">--}}
                        {{--<li><a href="about.html"><i class="glyphicon glyphicon-chevron-right"></i> Submenu 1</a></li>--}}
                        {{--<li><a href="about.html"><i class="glyphicon glyphicon-chevron-right"></i> Submenu 2</a></li>--}}
                        {{--<li><a href="about.html"><i class="glyphicon glyphicon-chevron-right"></i> Submenu 3</a></li>--}}
                    {{--</ul>--}}
                {{--</li>--}}
                {{--<li>--}}
                    {{--<a href="#"><span class="badge pull-right">731</span> Orders</a>--}}
                {{--</li>--}}
                {{--<li>--}}
                    {{--<a href="#"><span class="badge pull-right">812</span> Invoices</a>--}}
                {{--</li>--}}
                {{--<li>--}}
                    {{--<a href="#"><span class="badge pull-right">27</span> Clients</a>--}}
                {{--</li>--}}
                {{--<li>--}}
                    {{--<a href="#"><span class="badge pull-right">1,234</span> Users</a>--}}
                {{--</li>--}}
                {{--<li>--}}
                    {{--<a href="#"><span class="badge pull-right">2,221</span> Messages</a>--}}
                {{--</li>--}}
                {{--<li>--}}
                    {{--<a href="#"><span class="badge pull-right">11</span> Reports</a>--}}
                {{--</li>--}}
                {{--<li>--}}
                    {{--<a href="#"><span class="badge pull-right">83</span> Errors</a>--}}
                {{--</li>--}}
                {{--<li>--}}
                    {{--<a href="#"><span class="badge pull-right">4,231</span> Logs</a>--}}
                {{--</li>--}}
            </ul>
        </div>

        <!-- content -->
        @yield('content')
        <!-- content -->

    </div>
</div>

<!-- footer -->
<div class="navbar navbar-footer">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <footer role="contentinfo">
                    <p class="left">Hc4u Admin panel</p>
                    <p class="right">&copy; 2016 <a href="http://www.hc4ubd.com" target="_blank">hc4ubd.com</a></p>
                </footer>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="http://code.jquery.com/jquery-2.0.3.min.js"></script>
{!! HTML::script('assets/admin_template/js/bootstrap.min.js') !!}
{!! HTML::script('assets/admin_template/js/twitter-bootstrap-hover-dropdown.min.js') !!}
{!! HTML::script('assets/admin_template/js/bootstrap-admin-theme-change-size.js') !!}
{!! HTML::script('assets/admin_template/plugins/datatables/js/jquery.dataTables.min.js') !!}
{!! HTML::script('assets/admin_template/js/DT_bootstrap.js') !!}
{!! HTML::script('assets/admin_template/plugins/tinymce/js/tinymce/tinymce.min.js') !!}
{!! HTML::script('assets/admin_template/plugins/easypiechart/jquery.easy-pie-chart.js') !!}



{{--<script type="text/javascript" src="vendors/tinymce/js/tinymce/tinymce.min.js"></script>--}}

{{--<script type="text/javascript" src="js/bootstrap.min.js"></script>--}}
{{--<script type="text/javascript" src="js/twitter-bootstrap-hover-dropdown.min.js"></script>--}}
{{--<script type="text/javascript" src="js/bootstrap-admin-theme-change-size.js"></script>--}}

<script type="text/javascript">

    $(function() {
        // TinyMCE Full
        tinymce.init({
            selector: "#tinymce_full, #tinymce_full1, #tinymce_full2, #tinymce_full3",
            plugins: [
                "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                "searchreplace wordcount visualblocks visualchars code fullscreen",
                "insertdatetime media nonbreaking save table contextmenu directionality",
                "emoticons template paste textcolor"
            ],
            toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
            toolbar2: "print preview media | forecolor backcolor emoticons",
            image_advtab: true,
            templates: [
                {title: 'Test template 1', content: 'Test 1'},
                {title: 'Test template 2', content: 'Test 2'}
            ]
        });
    });


    $(function() {
        // Easy pie charts
        $('.easyPieChart').easyPieChart({animate: 1000});
    });

</script>


</body>
</html>
