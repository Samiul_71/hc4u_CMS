-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 27, 2016 at 05:59 PM
-- Server version: 5.5.50-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `hc4u`
--

-- --------------------------------------------------------

--
-- Table structure for table `about_us`
--

CREATE TABLE IF NOT EXISTS `about_us` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `image` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_by` tinyint(1) NOT NULL,
  `updated_by` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `about_us`
--

INSERT INTO `about_us` (`id`, `title`, `content`, `image`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'About Us', '<pre style="background-color: #ffffff; color: #000000; font-family: ''DejaVu Sans Mono''; font-size: 9.0pt;">This is Text<br />This is Text<br />This is Text</pre>', 'uploads/about/82273.png', 1, 0, 0, '2016-07-25 11:31:42', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `achivement`
--

CREATE TABLE IF NOT EXISTS `achivement` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `sub_title` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_by` tinyint(1) NOT NULL,
  `updated_by` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `achivement`
--

INSERT INTO `achivement` (`id`, `title`, `sub_title`, `image`, `description`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'Health Care For You (Pvt) Ltd. has two products:', 'Hc4u', 'uploads/achivement/40248.png', '<p>new health</p>', 1, 0, 0, '2016-07-27 10:54:40', '0000-00-00 00:00:00'),
(2, '', 'TeleDaktar', 'uploads/achivement/64797.png', '<p>This is text</p>', 1, 0, 0, '2016-07-27 10:55:21', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `competition`
--

CREATE TABLE IF NOT EXISTS `competition` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `image1` varchar(255) NOT NULL,
  `image2` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_by` tinyint(1) NOT NULL,
  `updated_by` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `competition`
--

INSERT INTO `competition` (`id`, `title`, `image`, `image1`, `image2`, `description`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'Grameenphone Accelerator', 'uploads/competition/39931.jpg', 'uploads/competition/648975.jpg', 'uploads/competition/855190.jpg', '<p>Test</p>', 1, 0, 0, '2016-07-26 11:16:33', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `condition`
--

CREATE TABLE IF NOT EXISTS `condition` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_by` tinyint(1) NOT NULL,
  `updated_by` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `condition`
--

INSERT INTO `condition` (`id`, `title`, `description`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'as', 'asd', 1, 1, 1, '2016-07-27 09:57:37', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE IF NOT EXISTS `contact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `created_by` tinyint(1) NOT NULL,
  `updated_by` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `contact`
--

INSERT INTO `contact` (`id`, `title`, `description`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'Contact Us', '<p>Health Care for You (Pvt) Ltd.<br /> Park Stone (Ground Floor)<br /> House# 32, Road# 7, Block &ndash; F,<br /> Banani, Dhaka &ndash; 1213<br /> Bangladesh<br /> Tel: +88 02 9872426, +88 02 9872905<br /> Website: <a href="http://www.hc4ubd.com" target="_blank">www.hc4ubd.com,</a> <a href="http://healthcare4umember.com" target="_blank">www.healthcare4umember.com</a></p>', 0, 0, '2016-07-27 05:46:45', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `corporation`
--

CREATE TABLE IF NOT EXISTS `corporation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `photo` varchar(255) NOT NULL,
  `text` text NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_by` tinyint(1) NOT NULL,
  `updated_by` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `corporation`
--

INSERT INTO `corporation` (`id`, `title`, `image`, `photo`, `text`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'This is new title1', 'uploads/corporation/85661.png', '', '<p>This is text1</p>', 1, 0, 0, '2016-07-26 11:05:56', '0000-00-00 00:00:00'),
(2, 'InfoMed, Malaysian Premier Healthcare Magazine', 'uploads/corporation/47755.png', '', '<pre style="background-color: #ffffff; color: #000000; font-family: ''DejaVu Sans Mono''; font-size: 9.0pt;"><span style="color: #808080; font-style: italic;"> We strongly believe that healthcare should be both personal and professional responsibility.<br /></span><span style="color: #808080; font-style: italic;"> We make it our mission to provide&nbsp;engaging and diversified coverage on government policies &amp;&nbsp;programs in Malaysia and around the world, as well as the latest&nbsp;clinical studies &amp;&nbsp;research findings, and of course, personal health.<br /></span><span style="color: #808080; font-style: italic;"> Our extensive reach includes doctors, healthcare professionals and the public.</span></pre>', 1, 0, 0, '2016-07-26 11:04:48', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `csr`
--

CREATE TABLE IF NOT EXISTS `csr` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `image1` varchar(255) NOT NULL,
  `image2` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `created_by` tinyint(1) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `updated_by` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `csr`
--

INSERT INTO `csr` (`id`, `title`, `image`, `image1`, `image2`, `description`, `created_by`, `status`, `updated_by`, `created_at`, `updated_at`) VALUES
(7, 'This is Title', 'uploads/csr/13806.jpg', 'uploads/csr/26146.jpg', 'uploads/csr/74852.jpg', '<p>wsadf</p>', 0, 1, 0, '2016-07-26 10:09:51', '0000-00-00 00:00:00'),
(8, 'This is Title', 'uploads/csr/97542.jpg', 'uploads/csr/718616.jpg', 'uploads/csr/39061.jpg', '<p>ad</p>', 0, 1, 0, '2016-07-26 10:10:06', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `elibrary`
--

CREATE TABLE IF NOT EXISTS `elibrary` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_by` tinyint(1) NOT NULL,
  `updated_by` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `elibrary`
--

INSERT INTO `elibrary` (`id`, `title`, `description`, `image`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'Mango For Health', '<pre style="background-color: #ffffff; color: #000000; font-family: ''DejaVu Sans Mono''; font-size: 9.0pt;">All members will get daily health tips via our App/portal<br />To receive eHealth \r\n                                            </pre>', 'uploads/elibrary/67924.jpg', 1, 0, 0, '2016-07-27 11:33:49', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `emagazine`
--

CREATE TABLE IF NOT EXISTS `emagazine` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_by` tinyint(1) NOT NULL,
  `updated_by` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `emagazine`
--

INSERT INTO `emagazine` (`id`, `title`, `image`, `description`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(5, 'InfoMed, Malaysian Premier Healthcare Magazine', 'uploads/emagazine/21426.png', '<pre style="background-color: #ffffff; color: #000000; font-family: ''DejaVu Sans Mono''; font-size: 9.0pt;">We strongly believe that healthcare should be both personal and professional responsibility.<br />We make it our mission to provide&nbsp;engaging an', 1, 0, 0, '2016-07-27 06:40:32', '0000-00-00 00:00:00'),
(6, 'Test', 'uploads/emagazine/63983.png', '<p><a href="http://hc4ubd.com/tools.html#health_tips" target="_blank">hc4u</a></p>', 1, 0, 0, '2016-07-27 08:26:58', '0000-00-00 00:00:00'),
(7, 'Test', 'uploads/emagazine/62330.png', '<p>aljsdaslkdjlasjdlasjdlkasjdlkasjdlkasjdljasslkdjaslkdjalksd</p>\r\n<p><a href="http://hc4ubd.com/tools.html#health_tips">aasa</a></p>\r\n<p>asda</p>', 1, 0, 0, '2016-07-27 08:27:46', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `establishment`
--

CREATE TABLE IF NOT EXISTS `establishment` (
  `id` tinyint(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `sub_title` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_by` tinyint(1) NOT NULL,
  `updated_by` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `establishment`
--

INSERT INTO `establishment` (`id`, `title`, `sub_title`, `image`, `description`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'Health Care For You (Pvt) Ltd. has been established on the 22nd day of May 2015', 'TAKE CHARGE OF YOUR OWN HEALTH', 'uploads/establishment/75219.png', '<p>text</p>', 1, 0, 0, '2016-07-27 10:05:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `fair`
--

CREATE TABLE IF NOT EXISTS `fair` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `image1` varchar(255) NOT NULL,
  `image2` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_by` tinyint(1) NOT NULL,
  `updated_by` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `fair`
--

INSERT INTO `fair` (`id`, `title`, `image`, `image1`, `image2`, `description`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(8, 'Malaysian International Health Fair (2015)', 'uploads/fair/168655.jpg', 'uploads/fair/18870.jpg', 'uploads/fair/49987.jpg', '<p>test</p>', 1, 0, 0, '2016-07-27 06:11:12', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `gallary`
--

CREATE TABLE IF NOT EXISTS `gallary` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `status` int(11) NOT NULL,
  `created_by` tinyint(1) NOT NULL,
  `updated_by` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `gallary`
--

INSERT INTO `gallary` (`id`, `title`, `image`, `content`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(3, 'Test', 'uploads/gallary/67662.jpg', 'test', 1, 0, 0, '2016-07-25 08:19:34', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `hospital`
--

CREATE TABLE IF NOT EXISTS `hospital` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `address` text NOT NULL,
  `city` varchar(255) NOT NULL,
  `district` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `discount` text NOT NULL,
  `number` int(255) NOT NULL,
  `created_by` tinyint(1) NOT NULL,
  `updated_by` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `hospital`
--

INSERT INTO `hospital` (`id`, `name`, `address`, `city`, `district`, `image`, `description`, `status`, `discount`, `number`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'IB', '<p>Dhaka</p>', 'Dhaka', 'Dhaka', 'uploads/hospital/28326.jpg', '<p>Health</p>', 1, '<p>40%</p>', 0, 0, 0, '2016-07-25 04:51:16', '0000-00-00 00:00:00'),
(2, 'admina', '<p>sdas</p>', 'Barishal', 'asd', 'uploads/gallary/53223.png', 'asd', 1, 'asd', 0, 0, 0, '2016-07-21 10:42:14', '0000-00-00 00:00:00'),
(3, 'popular', '<p>Rangpur</p>', 'Rangpur', 'Rangpur', 'uploads/hospital/72109.jpg', '<p>Health</p>', 1, '<p>30%</p>', 0, 0, 0, '2016-07-25 04:54:46', '0000-00-00 00:00:00'),
(4, 'admin', '<p>as</p>', 'Dhaka', 'asd', 'uploads/hospital/93238.jpg', '<p>asd</p>', 1, '<p>asd</p>', 0, 0, 0, '2016-07-24 04:06:20', '0000-00-00 00:00:00'),
(5, 'admin', '<p>sd</p>', 'Barishal', 'sdf', 'uploads/hospital/86369.jpg', '<p>sdf</p>', 1, '<p>sdf</p>', 0, 0, 0, '2016-07-26 08:17:47', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `logo`
--

CREATE TABLE IF NOT EXISTS `logo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_by` tinyint(1) NOT NULL,
  `updated_by` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `logo`
--

INSERT INTO `logo` (`id`, `name`, `image`, `description`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(2, 'Hc4u', 'uploads/logo/43879.png', '<p>test</p>', 1, 0, 0, '2016-07-26 06:11:24', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `main_products`
--

CREATE TABLE IF NOT EXISTS `main_products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_by` tinyint(1) NOT NULL,
  `updated_by` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `main_products`
--

INSERT INTO `main_products` (`id`, `title`, `image`, `description`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'This is new title', 'uploads/main_product/28963.png', '<p>This is text</p>', 1, 0, 0, '2016-07-26 06:01:31', '0000-00-00 00:00:00'),
(2, '', 'uploads/main_product/33665.png', '', 1, 0, 0, '2016-07-26 06:01:47', '0000-00-00 00:00:00'),
(3, 'Test', 'uploads/main_product/97265.png', '<pre style="background-color: #ffffff; color: #000000; font-family: ''DejaVu Sans Mono''; font-size: 9.0pt;">HC4U is a cloud based platform that collects and records personal health information of <br />users through Doctors,Labs, Hospitals.<br /><br /><a h', 1, 0, 0, '2016-07-27 07:25:37', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `others_products`
--

CREATE TABLE IF NOT EXISTS `others_products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_by` tinyint(1) NOT NULL,
  `updated_by` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `others_products`
--

INSERT INTO `others_products` (`id`, `title`, `image`, `description`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'Test', 'uploads/other_product/79293.jpg', '<p>test</p>', 1, 0, 0, '2016-07-24 04:25:50', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `publication`
--

CREATE TABLE IF NOT EXISTS `publication` (
  `id` tinyint(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `sub_title` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_by` tinyint(1) NOT NULL,
  `updated_by` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `publication`
--

INSERT INTO `publication` (`id`, `title`, `sub_title`, `image`, `description`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'This is new title', 'Prothom-Alo', 'uploads/publication/96831.jpg', '<p>This is text</p>', 1, 0, 0, '2016-07-25 07:11:05', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `slider`
--

CREATE TABLE IF NOT EXISTS `slider` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_by` tinyint(1) NOT NULL,
  `updated_by` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `slider`
--

INSERT INTO `slider` (`id`, `name`, `image`, `description`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(2, 'This is new', 'uploads/slider/29455.jpg', '<p>alskfdalksdfjalskjdkla</p>', 1, 0, 0, '2016-07-25 10:33:38', '0000-00-00 00:00:00'),
(3, '', 'uploads/slider/43798.jpg', '', 1, 0, 0, '2016-07-26 07:00:12', '0000-00-00 00:00:00'),
(4, '', 'uploads/slider/45422.jpg', '', 1, 0, 0, '2016-07-26 06:11:58', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tool`
--

CREATE TABLE IF NOT EXISTS `tool` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `flag` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_by` tinyint(1) NOT NULL,
  `updated_by` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tool`
--

INSERT INTO `tool` (`id`, `flag`, `title`, `content`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, '', 'asd', 'asd', 1, 1, 1, '2016-07-27 08:53:24', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tools`
--

CREATE TABLE IF NOT EXISTS `tools` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `flag` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `created_by` tinyint(1) NOT NULL,
  `updated_by` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `tools`
--

INSERT INTO `tools` (`id`, `flag`, `title`, `content`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(2, 'ephr', 'ePHR', '<pre style="background-color: #ffffff; color: #000000; font-family: ''DejaVu Sans Mono''; font-size: 9.0pt;">Member can preserve their Medical records electronically in HC4U Health App &amp; Portal.&lt;<span style="color: #000080; font-weight: bold;">br</span>&gt;<br />Those data can be accessed virtually from anywhere anytime.&lt;<span style="color: #000080; font-weight: bold;">br</span>&gt;<br />To create your personal ePHR, please click the following links:&lt;<span style="color: #000080; font-weight: bold;">br</span>&gt;<br />For subscribed members: <a href="http://healthcare4umember.com/HC4U/login">healthcare4umember/HC4U/login</a><br />For subscription: <a href="http://healthcare4umember.com/HC4U/signup">healthcare4umember/HC4U/signup</a></pre>', 0, 0, '2016-07-27 09:32:20', '0000-00-00 00:00:00'),
(3, 'book_appointment', 'Book Appointment', '<pre style="background-color: #ffffff; color: #000000; font-family: ''DejaVu Sans Mono''; font-size: 9.0pt;">Members can make Appointment with our panel Doctors by our Health App and Portal as well as Short Code.&lt;<span style="color: #000080; font-weight: bold;">br</span>&gt;<br />To Book an appointment with your desired doctor, please click the following links:&lt;<span style="color: #000080; font-weight: bold;">br</span>&gt;<br />For subscribed members: &lt;<span style="color: #000080; font-weight: bold;">a </span>href=<span style="color: #008000; font-weight: bold;">"http://healthcare4umember.com/HC4U/login"</span>&gt;http://healthcare4umember.com/HC4U/login&lt;/<span style="color: #000080; font-weight: bold;">a</span>&gt;&lt;<span style="color: #000080; font-weight: bold;">br</span>&gt;<br />For subscription: &lt;<span style="color: #000080; font-weight: bold;">a </span>href=<span style="color: #008000; font-weight: bold;">"http://healthcare4umember.com/HC4U/signup"</span>&gt;http://healthcare4umember.com/HC4U/signup&lt;/<span style="color: #000080; font-weight: bold;">a</span>&gt;</pre>', 0, 0, '2016-07-27 06:46:02', '0000-00-00 00:00:00'),
(4, 'tele_medicine', 'Tele-Medicine', '<pre style="background-color: #ffffff; color: #000000; font-family: ''DejaVu Sans Mono''; font-size: 9.0pt;">Through Tele-health service the Member can get consultation from Doctors.&lt;<span style="color: #000080; font-weight: bold;">br</span>&gt;<br />It is important to get consultancy from the physicians from time to time.&lt;<span style="color: #000080; font-weight: bold;">br</span>&gt;<br />Our members will get free consultancy from our in-house General Physician.&lt;<span style="color: #000080; font-weight: bold;">br</span>&gt;<br />To receive Tele-Medicine services, please click the following links:&lt;<span style="color: #000080; font-weight: bold;">br</span>&gt;<br />For subscribed members: &lt;<span style="color: #000080; font-weight: bold;">a </span>href=<span style="color: #008000; font-weight: bold;">"http://healthcare4umember.com/HC4U/login"</span>&gt;http://healthcare4umember.com/HC4U/login&lt;/<span style="color: #000080; font-weight: bold;">a</span>&gt;&lt;<span style="color: #000080; font-weight: bold;">br</span>&gt;<br />For subscription: &lt;<span style="color: #000080; font-weight: bold;">a </span>href=<span style="color: #008000; font-weight: bold;">"http://healthcare4umember.com/HC4U/signup"</span>&gt;http://healthcare4umember.com/HC4U/signup&lt;/<span style="color: #000080; font-weight: bold;">a</span>&gt;</pre>', 0, 0, '2016-07-27 06:47:03', '0000-00-00 00:00:00'),
(5, 'my_insurance', 'My Insurance', '<pre style="background-color: #ffffff; color: #000000; font-family: ''DejaVu Sans Mono''; font-size: 9.0pt;">It is important to procure an insurance package for emergencies.&lt;<span style="color: #000080; font-weight: bold;">br</span>&gt;<br />Our members can avail all these service from anywhere in Bangladesh.&lt;<span style="color: #000080; font-weight: bold;">br</span>&gt;<br />In-hospital medical benefits          : 10,000 BDT&lt;<span style="color: #000080; font-weight: bold;">br</span>&gt;<br />Accidental Death Insurance        : 10,000 BDT&lt;<span style="color: #000080; font-weight: bold;">br</span>&gt;<br />Permanent Disability Insurance         : 10,000 BDT&lt;<span style="color: #000080; font-weight: bold;">br</span>&gt;<br />To receive an insurance package, please click the following links:&lt;<span style="color: #000080; font-weight: bold;">br</span>&gt;<br />For subscribed members: &lt;<span style="color: #000080; font-weight: bold;">a </span>href=<span style="color: #008000; font-weight: bold;">"http://healthcare4umember.com/HC4U/login"</span>&gt;http://healthcare4umember.com/HC4U/login&lt;/<span style="color: #000080; font-weight: bold;">a</span>&gt;&lt;<span style="color: #000080; font-weight: bold;">br</span>&gt;<br />For subscription: &lt;<span style="color: #000080; font-weight: bold;">a </span>href=<span style="color: #008000; font-weight: bold;">"http://healthcare4umember.com/HC4U/signup"</span>&gt;http://healthcare4umember.com/HC4U/signup&lt;/<span style="color: #000080; font-weight: bold;">a</span>&gt;</pre>', 0, 0, '2016-07-27 06:47:32', '0000-00-00 00:00:00'),
(6, 'discount', 'Discount at Hospital', '<pre style="background-color: #ffffff; color: #000000; font-family: ''DejaVu Sans Mono''; font-size: 9.0pt;">Member and their dependents can enjoy discount up-to a certain percentage from many Major Private Hospitals and Diagnostics centers within HC4U panel.&lt;<span style="color: #000080; font-weight: bold;">br</span>&gt;<br />To avail Discounts at Hospitals, package please clicks the following links:&lt;<span style="color: #000080; font-weight: bold;">br</span>&gt;<br />For subscribed members: &lt;<span style="color: #000080; font-weight: bold;">a </span>href=<span style="color: #008000; font-weight: bold;">"http://healthcare4umember.com/HC4U/login"</span>&gt;http://healthcare4umember.com/HC4U/login&lt;/<span style="color: #000080; font-weight: bold;">a</span>&gt;&lt;<span style="color: #000080; font-weight: bold;">br</span>&gt;<br />For subscription: &lt;<span style="color: #000080; font-weight: bold;">a </span>href=<span style="color: #008000; font-weight: bold;">"http://healthcare4umember.com/HC4U/signup"</span>&gt;http://healthcare4umember.com/HC4U/signup&lt;/<span style="color: #000080; font-weight: bold;">a</span>&gt;</pre>', 0, 0, '2016-07-27 06:48:06', '0000-00-00 00:00:00'),
(7, 'health_tips', 'eHealth Tips', '<pre style="background-color: #ffffff; color: #000000; font-family: ''DejaVu Sans Mono''; font-size: 9.0pt;">All members will get daily health tips via our App/portal&lt;<span style="color: #000080; font-weight: bold;">br</span>&gt;<br />To receive eHealth Tips, please click the following links:&lt;<span style="color: #000080; font-weight: bold;">br</span>&gt;<br />For subscribed members: &lt;<span style="color: #000080; font-weight: bold;">a </span>href=<span style="color: #008000; font-weight: bold;">"http://healthcare4umember.com/HC4U/login"</span>&gt;http://healthcare4umember.com/HC4U/login&lt;/<span style="color: #000080; font-weight: bold;">a</span>&gt;&lt;<span style="color: #000080; font-weight: bold;">br</span>&gt;<br />For subscription: &lt;<span style="color: #000080; font-weight: bold;">a </span>href=<span style="color: #008000; font-weight: bold;">"http://healthcare4umember.com/HC4U/signup"</span>&gt;http://healthcare4umember.com/HC4U/signup&lt;/<span style="color: #000080; font-weight: bold;">a</span>&gt;</pre>', 0, 0, '2016-07-27 06:48:28', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tool_bk`
--

CREATE TABLE IF NOT EXISTS `tool_bk` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `flag` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `created_by` tinyint(1) NOT NULL,
  `updated_by` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Table structure for table `upcoming_event`
--

CREATE TABLE IF NOT EXISTS `upcoming_event` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_by` tinyint(1) NOT NULL,
  `updated_by` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `upcoming_event`
--

INSERT INTO `upcoming_event` (`id`, `title`, `image`, `description`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(4, 'Test', 'uploads/upcoming_event/28072.png', '<p>HC4U is taking part in E27, Echelon Asia Summit 2016, where HC4U is currently holding position number 9 out of 100 in this event.<br /> International Health Fair &ndash; HC4U is intending to host an International Health Fair for 3 continuous days in November 2016</p>', 1, 0, 0, '2016-07-26 12:02:51', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin@gmail.com', '$2y$10$rY/iUCDIuHMB0IHNR.6GGOwLE3MHPRxDZIJsNKtZ2K97qoTZm3gEW', '4jySiqfRw9NdGFJnKIF3eBwoYWbM5CwjhPXA5F9Ow3UIcyleKWGp3kskBqQS', '2016-07-19 00:12:13', '2016-07-25 03:52:36');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
