<?php
require_once 'db.php';
require_once "header.php";

?>

<div class="box">
    <div class="slick-slider">
        <div id="sliderFrame">
            <?php
            $sql = "SELECT * FROM slider WHERE status=1";
            $result3 = mysqli_query($link, $sql);
            while ($row = mysqli_fetch_assoc($result3)) {
                $data4[] = $row;
            }
            ?>

            <div id="slider">
                <?php
                if (!empty($data4)) {
                foreach ($data4 as $item4) {
                ?>
                <img src="admin/public/<?php echo $item4['image'];?>"/>
<!--                <img src="img/slide/2.jpg"/>-->
<!--                <img src="img/slide/3.jpg"/>-->
<!--                <img src="img/slide/4.jpg"/>-->
<!--                <img src="img/slide/5.jpg"/>-->
<!--                <img src="img/slide/6.jpg"/>-->
                <?php }}?>
            </div>

        </div>
    </div>

    <div class="wrapper">

        <section id="about">
            <div class="container">
                <?php
                $sql = "SELECT * FROM about_us WHERE status=1";
                $result1 = mysqli_query($link, $sql);
                while ($row1 = mysqli_fetch_assoc($result1)) {
                    $data1[] = $row1;
                }
                ?>
                <div class="section-heading scrollpoint sp-effect3">

                    <h1>About Us</h1>
                    <div class="divider"></div>
                    <p>Health Care For You (Pvt) Ltd. (HC4U)</p>
                </div>

                <div class="row">
                    <div class="col-md-8 col-sm-8">
                        <?php
                        if (!empty($data1)) {
                            foreach ($data1 as $item1) {
                                ?>
                        <h3><?php echo  $item1['title'];?></h3>
                        <?php echo json_encode($item1['content']);?>
<!--                        <p>-->
<!--                            Heath Care For You (Pvt) Ltd., a technology based company, is working on the next generation of healthcare for the nation. We have developed a web-based mobile application, HC4U.-->
<!--                            <br><br>-->
<!--                            HC4U is an IT platform to empower everyone for taking charge of his/her own health using mHealth. This platform integrates health-care seekers with healthcare providers such as hospitals, doctors and insurance companies under one umbrella offering opportunity for a paperless & cashless treatment for every individual under the universal wellness program.-->
<!--                            <br><br>-->
<!---->
<!--                            HC4U is a movement to develop national health backbone with personal healthcare records of every citizen into an inter-operable system. It raises awareness regarding maintaining health records and its benefit in the effective management of clinical support. This service is for all citizens from anywhere and everywhere. Hc4U’s target is to bring citizenry into a smart treatment systems based on electronic records of health status to ensure treatment without delay in a cost effective manner.-->
<!--                            <br><br>-->
<!--                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-->
<!--                            <q>-->
<!--                                <strong>-->
<!--                                    Health is a right, not a privilege. It needs to be enjoyed with equity.-->
<!--                                </strong>-->
<!--                            </q>-->
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <div class="about-item scrollpoint sp-effect2">
                            <img src="admin/public/<?php echo $item1['image'];?>"">
                            <?php }}?>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section id="features">
            <div class="container">
                <br><br>
                <div class="row">
                    <div class="tools" style="text-align: center;">
                        <ul id="categories" class="clr" style="height:680px; width: 780px;">
                            <li class="pusher"></li>
                            <li class="hex">
                                <a class="hexIn" href="tools.php#ephr">
                                    <img src="img/ePHR.png" alt=""/>
                                </a>
                            </li>
                            <li class="hex">
                                <a class="hexIn" href="tools.php#tele_medicine">
                                    <img src="img/tele_medicine.png" alt=""/>
                                </a>
                            </li>
                            <li class="hex">
                                <a class="hexIn" href="tools.php#book_appointment">
                                    <img src="img/book_apoint.png" alt=""/>
                                </a>
                            </li>
                            <li class="hex">
                                <a class="hexIn" href="tools.php">
                                    <img src="img/healthcare_tools.png" alt=""/>
                                </a>
                            </li>
                            <li class="hex">
                                <a class="hexIn" href="tools.php#discount">
                                    <img src="img/hospital_discounts.png" alt=""/>
                                </a>
                            </li>
                            <li class="pusher"></li>
                            <li class="hex">
                                <a class="hexIn" href="tools.php#my_insurance">
                                    <img src="img/my_insurance.png" alt=""/>
                                </a>
                            </li>
                            <li class="hex">
                                <a class="hexIn" href="tools.php#health_tips">
                                    <img src="img/ehealth_tips.png" alt=""/>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>

        <div id="achieve">
            <div class="container">
                <div class="row vdivide"><br>
                    <div class="col-sm-3 text-center">
                        <a href="establishment.php#establish" style="text-decoration: none;"><h4><strong>Year of Establishment</strong></h4></a>
                    </div>
                    <div class="col-sm-3 text-center">
                        <a href="establishment.php#achievement" style="text-decoration: none;"><h4><strong>Achievements</strong></h4></a>
                    </div>
                    <div class="col-sm-3 text-center">
                        <a href="events.php#upcoming_events" style="text-decoration: none;"><h4><strong>Upcoming Events</strong></h4></a>
                    </div>
                    <div class="col-sm-3 text-center">
                        <a href="establishment.php#publication" style="text-decoration: none;"><h4><strong>Publications</strong></h4></a>
                    </div>
                </div>
            </div>
            <br>
            <hr>
            <div class="container">
                <div class="row" id="pre-footer">
                    <div class="col-sm-5 col-md-5 form-inline text-center">
                        <label> News Letter:</label>
                        <input type="text" name="" placeholder="  Please enter your Email Address for Newsletter" style="border-radius: 10px; width: 330px; height: 30px;">
                    </div>
                    <div class="col-sm-3 col-md-3 text-center">
                        <div class="row text-center">
                            <a href="https://itunes.apple.com/gb/app/hc4u/id1038842422?mt=8" target="_blank" style="text-decoration: none;">
                                <img src="img/ios.png">
                            </a>
                            <a href="https://play.google.com/store/apps/details?id=com.healthcare.hc4u&hl=en" target="_blank" style="text-decoration: none;">
                                <img src="img/android.png">
                            </a>
                        </div>
                    </div>
                    <div class="col-sm-4 col-md-4 form-inline text-center">
                        <label>Social Links: </label>&nbsp;&nbsp;
                        <a href="https://twitter.com/Hc4ubd" target="_blank" style="text-decoration: none;">
                            <span><i class="fa fa-twitter-square fa-3x" aria-hidden="true"></i></span>
                            <!--<img src="img/fb.png">-->
                        </a>&nbsp;&nbsp;
                        <a href="https://www.facebook.com/hc4ubd" target="_blank" style="text-decoration: none;">
                            <span><i class="fa fa-facebook-square fa-3x" aria-hidden="true"></i></span>
                            <!--<img src="img/twitter.png">-->
                        </a>
                    </div>
                </div><br>
            </div>
        </div>
    </div>
</div>

<?php require_once 'footer.php'; ?>

<script src="js/jquery-1.11.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/slick.min.js"></script>
<script src="js/placeholdem.min.js"></script>
<script src="js/rs-plugin/js/jquery.themepunch.plugins.min.js"></script>
<script src="js/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
<script src="js/waypoints.min.js"></script>
<script src="js/scripts.js"></script>
<script>
    $(document).ready(function() {
        appMaster.preLoader();
    });
</script>

</body>
</html>
