<?php require_once 'header.php';?>

    <div class="wrapper">
        <section id="establishment">
            <div class="container">
                <div class="row" id="establish">
                    <div class="col-md-12 col-sm-12">
                        <?php
                        require_once 'db.php';
                        $sql = "SELECT * FROM establishment WHERE status=1";

                        $result3 = mysqli_query($link, $sql);

                        while ($row = mysqli_fetch_assoc($result3)) {
                            $data2[] = $row;
//                            print_r($row);
                        }
                        ?>

                        <h3>Year of Establishment</h3>
                        <?php
                        if (!empty($data2)) {
                            foreach ($data2 as $item) {
                                ?>
                        <hr>
                        <p><?php echo $item['title'];?></p>
                        <h4><?php echo $item['sub_title'];?></h4>
                        <img src="admin/public/<?php echo $item['image'];?>" height="450px" width="600px">
                        <?php }} ?>
                    </div>
                </div>
            </div><br>
            <div class="container">
                <div class="row" id="achievement">
                    <div class="col-md-12 col-sm-12">
                        <h2>Achievement</h2>
                        <?php
                        require_once 'db.php';
                        $sql = "SELECT * FROM achivement WHERE status=1";
                        $result3 = mysqli_query($link, $sql);
//                        print_r($result3);
                        while ($row = mysqli_fetch_assoc($result3)) {
                            $data4[] = $row;


                        }
                        ?>

                        <hr>
<!--                        <p>Products of Health Care For You (Pvt) Ltd.</p><br>-->
                        <?php
                        if (!empty($data4)) {
                            foreach ($data4 as $item) {
                              ?>
                        <ul>
                             <li><?php echo $item['sub_title'];?></li>
                             <img src="admin/public/<?php echo $item['image'];?>" style="height: 80px; width: 100px;">
                             <p><?php echo $item['description'];?></p>
                        </ul>
                                <hr>
                            <?php }} ?>
                        <span class="glyphicon glyphicon-th-large"></span>
                        Took Part in SD Asia, Grameenphone Accelerator and became a winner<br>
<!--                        <img src="img/gp_accelerator.png" style="height: 80px; width: 150px;"><br><br>-->
<!--                        <span class="glyphicon glyphicon-th-large"></span>-->
<!--                        Took Part in E27, Echelon Summit and reached at top 9 finalist out of 100 participants<br>-->
<!--                        <a href="https://e27.co/top100-startups-from-bangladesh-and-india-wholl-be-at-ecasia2016-20160525/" target="_blank">-->
<!--                            <img src="img/e27_light.png" style="height: 50px; width: 110px;">-->
<!--                        </a>-->
<!--                        <br>-->
<!--                        <a href="https://e27.co/top100-startups-from-bangladesh-and-india-wholl-be-at-ecasia2016-20160525/" target="_blank" style="text-decoration: none;">-->
<!--                            https://e27.co/top100-startups-from-bangladesh-and-india-wholl-be-at-ecasia2016-20160525/-->
<!--                        </a>-->
                    </div>
                </div>
            </div><br><br>
            <div class="container">
                <div class="row" id="publication">
                    <h2>Publication</h2>
                    <div class="col-md-12">
                        <p>This is great to see that    <a href="https://www.dailyinqilab.com/">dailyinqilab</a> Publish about Our Company</p>
                        <img src="img/news/inqilab.jpg" height="600" width="650">
                    </div>
                    <div class="col-md-5">
                        <img src="img/news/jugantor.jpg" height="600" width="450">
                    </div>
                </div>
                </div>
            </div>
        </section>
    </div>

    <div id="achieve">
        <div class="container">
            <br>
            <div class="container">
                <div class="row" id="pre-footer">
                    <div class="col-sm-5 col-md-5 form-inline text-center">
                        <label> News Letter:</label>
                        <input type="text" name="" placeholder="  Please enter your Email Address for Newsletter" style="border-radius: 10px; width: 330px; height: 30px;">
                    </div>
                    <div class="col-sm-3 col-md-3 text-center">
                        <div class="row text-center">
                            <a href="https://itunes.apple.com/gb/app/hc4u/id1038842422?mt=8" target="_blank" style="text-decoration: none;">
                                <img src="img/ios.png">
                            </a>
                            <a href="https://play.google.com/store/apps/details?id=com.healthcare.hc4u&hl=en" target="_blank" style="text-decoration: none;">
                                <img src="img/android.png">
                            </a>
                        </div>
                    </div>
                    <div class="col-sm-4 col-md-4 form-inline text-center">
                        <label>Social Links: </label>&nbsp;&nbsp;
                        <a href="https://twitter.com/Hc4ubd" target="_blank" style="text-decoration: none;">
                            <span><i class="fa fa-twitter-square fa-3x" aria-hidden="true"></i></span>
                        </a>&nbsp;&nbsp;
                        <a href="https://www.facebook.com/hc4ubd" target="_blank" style="text-decoration: none;">
                            <span><i class="fa fa-facebook-square fa-3x" aria-hidden="true"></i></span>
                        </a>
                    </div>
                </div><br>
            </div>
        </div>
    </div>

    <footer>
        <div class="container">
            <div class="row text-left">
                <div class="col-md-4">
                    <h3>Health Care</h3>
                    <ul>
                        <li><a href="http://hc4ubd.com/magazine" style="text-decoration: none">eHealth Magazine</a></li>
                        <li><a href="http://hc4ubd.com/library" style="text-decoration: none">eHealth Library</a></li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <h3>Information</h3>
                    <ul>
                        <li><a href="aboutus.php" style="text-decoration: none">About Us</a></li>
                        <li><a href="aboutus.php#contact" style="text-decoration: none">Contact Us</a></li>
                        <li><a href="aboutus.php#condition" style="text-decoration: none">Terms and Conditions</a></li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <h3>My Account</h3>
                    <ul>
                        <li><a href="tools.php" style="text-decoration: none">My Benefits</a></li>
                        <li><a href="http://healthcare4umember.com/HC4U/login" style="text-decoration: none">Member E-Card</a></li>
                        <li><a href="http://healthcare4umember.com/HC4U/login" style="text-decoration: none">My Insurance</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>

    <script src="js/jquery-1.11.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/slick.min.js"></script>
    <script src="js/placeholdem.min.js"></script>
    <script src="js/rs-plugin/js/jquery.themepunch.plugins.min.js"></script>
    <script src="js/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
    <script src="js/waypoints.min.js"></script>
    <script src="js/scripts.js"></script>
    <script>
        $(document).ready(function() {
            appMaster.preLoader();
        });
    </script>
</body>

</html>
