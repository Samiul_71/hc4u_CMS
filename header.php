<?php include 'db.php';?>
<!doctype html>
<html lang="en" class="no-js">
<head>
    <meta charset="UTF-8">
    <title>HC4U - Health Care For You</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <link rel="shortcut icon" href="favicon2.png">
<!--    <link rel="stylesheet" href="css/bootstrap.css">-->

    <link rel="stylesheet" href="css/animate.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/slick.css">
    <link rel="stylesheet" href="js/rs-plugin/css/settings.css">

    <script type="text/javascript" src="js/modernizr.custom.32033.js"></script>

    <link href="/css/bootstrap-glyphicons.css" rel="stylesheet">

    <link rel="stylesheet" href="css/eco.css">
    <link href="css/style.css" rel="stylesheet" type="text/css">

    <!--CSS for the Image/Photo Slider -->
    <link href="css/slider/js-image-slider.css" rel="stylesheet" type="text/css" />
    <!--JS for the Image/Photo Slider -->
    <script src="js/slider/js-image-slider.js" type="text/javascript"></script>

    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">

    <link href="css/table/bootstrap.css">
    <link href="css/table/fresh-bootstrap-table.css" rel="stylesheet" type="text/css"/>

    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>

    <link rel="stylesheet" href="//blueimp.github.io/Gallery/css/blueimp-gallery.min.css">
    <link rel="stylesheet" href="css/image_gallery/bootstrap-image-gallery.min.css">

    <style type="text/css">

        #about {
            padding-top: 140px;
            margin-top: -140px;
        }
        #about:after {
            /*background-image: url('img/white_bg_about.png');*/
            content : "";
            display: block;
            position: absolute;
            top: 0;
            left: 0;
            background-image: url(img/bg_images/footer.jpg);
            width: 100%;
            height: 100%;
            opacity : 0.2;
            z-index: -1;
        }
        #products:after{
            content : "";
            display: block;
            position: absolute;
            top: 0;
            left: 0;
            background-image: url(img/bg_images/footer.jpg);
            width: 100%;
            height: 100%;
            opacity : 0.2;
            z-index: -1;
        }
        #achieve {
            position: relative;
            color: #d7a10a;
        }
        #achieve:after {
            content : "";
            display: block;
            position: absolute;
            top: 0;
            left: 0;
            background-image: url(img/bg_images/footer_bk.jpg);
            width: 100%;
            height: 100%;
            opacity : 0.9;
            z-index: -1;
        }
        #achieve h4{
            color: #d7a10a;
        }
        #pre-footer{
            color: #d7a10a;
        }
        #pre-footer input{
            color: black;
        }
        footer h4{
            color: white;
        }
        footer a{
            color: white;
        }

        #events:after{
            content : "";
            display: block;
            position: absolute;
            top: 0;
            left: 0;
            background-image: url(img/bg_images/footer.jpg);
            width: 100%;
            height: 100%;
            opacity : 0.2;
            z-index: -1;
        }
        #achieve {
            position: relative;
            color: #d7a10a;
        }
        #achieve:after {
            content : "";
            display: block;
            position: absolute;
            top: 0;
            left: 0;
            background-image: url(img/bg_images/footer.jpg);
            width: 100%;
            height: 100%;
            opacity : 0.9;
            z-index: -1;
        }
       

        #csr {
            padding-top: 140px;
            margin-top: -140px;
        }
        #fair {
            padding-top: 140px;
            margin-top: -140px;
        }
        #competition {
            padding-top: 140px;
            margin-top: -140px;
        }
        #upcoming_events {
            padding-top: 140px;
            margin-top: -140px;
        }
        #establishment:after{
            content : "";
            display: block;
            position: absolute;
            top: 0;
            left: 0;
            background-image: url(img/bg_images/footer.jpg);
            width: 100%;
            height: 100%;
            opacity : 0.2;
            z-index: -1;
        }
        #tools {
            position: relative;
        }
        #tools:after {
            content : "";
            display: block;
            position: absolute;
            top: 0;
            left: 0;
            background-image: url(img/tools_bg.jpg);
            width: 100%;
            height: 100%;
            opacity : 0.2;
            z-index: -1;
        }
        #ephr {
            padding-top: 140px;
            margin-top: -140px;
        }
        #book_appointment {
            padding-top: 140px;
            margin-top: -140px;
        }
        #tele_medicine {
            padding-top: 140px;
            margin-top: -140px;
        }
        #my_insurance {
            padding-top: 140px;
            margin-top: -140px;
        }
        #discount {
            padding-top: 140px;
            margin-top: -140px;
        }
        #health_tips {
            padding-top: 140px;
            margin-top: -140px;
        }
        #content:after {
            content : "";
            display: block;
            position: absolute;
            top: 0;
            left: 0;
            background-image: url(img/bg_images/footer.jpg);
            width: 100%;
            height: 100%;
            opacity : 0.2;
            z-index: -1;
        }
    </style>
    <style>
        #map {
            border: dashed;
            border-radius: 5px;
            width: 1000px;
            height: 300px;
        }
    </style>

</head>

<body>

<div class="pre-loader">
    <div class="load-con">
        <img src="img/logo_bk.png" class="animated fadeInDown" alt="">
        <div class="spinner">
            <div class="bounce1"></div>
            <div class="bounce2"></div>
            <div class="bounce3"></div>
        </div>
    </div>
</div>

<header>
    <?php
    $sql = "SELECT * FROM logo WHERE status=1";
    $result = mysqli_query($link, $sql);
    while ($row = mysqli_fetch_assoc($result)) {
    $data[] = $row;
    }
?>
    <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="fa fa-bars fa-lg"></span>
                </button>

                <a class="navbar-brand" href="index.php">
                    <?php
                    if (!empty($data)) {
                    foreach ($data as $item) {
                    ?>
                    <img src="admin/public/<?php echo $item['image'];?>" alt="" class="logo">
                    <?php }}?>
                </a>

            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <form class="navbar-form navbar-left" role="search">
                            <div class="form-group">
                                <i class="icon-search"></i>
                                <input type="text" placeholder="   Search" style="border-radius: 10px; width: 330px; height: 30px;">
                            </div>
                        </form>
                    </li>
                    <li><a href="index.php" class="glyphicon glyphicon-home"> Home </a></li>
                    <li>
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Partner<span class="caret"></span></a>
                        <ul class="dropdown-menu text-right">
                            <li class="text-right"><a href="hospitals.php">Hospital List</a></li>
                            <li class="text-right"><a href="corporation.php">Corporations</a></li>

                        </ul>
                    </li>
                    <li>
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Products<span class="caret"></span></a>
                        <ul class="dropdown-menu text-right">
                            <li class="text-right"><a href="products.php">HC4U</a></li>
                            <li class="text-right"><a href="other_products.php">others</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Events<span class="caret"></span></a>
                        <ul class="dropdown-menu text-right" id="dropdwon-menu">
                            <li class="text-right"><a href="events.php#csr">CSR</a></li>
                            <li class="text-right"><a href="events.php#fair">Fair</a></li>
                            <li class="text-right"><a href="events.php#competition">Competition</a></li>
                            <li class="text-right"><a href="events.php#upcoming_events">Upcoming Events</a></li>
                        </ul>
                    </li>
                    <li><a href="http://healthcare4umember.com">My Account</a></li>
                </ul>
            </div>
        </div>
    </nav>
</header>